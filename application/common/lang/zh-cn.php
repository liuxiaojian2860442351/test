<?php

return [
// 头部导航

   'head_support' => '支持',
   'head_about_us' => '关于我们',
   'head_contact' => '联系',
   'head_shopping' => '商城',
   'head_search_place' => '怎么能帮助你',
   'head_product_faqs' => '产品说明',
   'head_shopping_faqs' => '购买说明',
   'head_troubles' => '问题',
   'head_repair_service' => '售后服务',
   'head_software_upgrade' => '软件升级',
   'head_user_manual' => '用户手册',
// 底部导航
   'foot_buy' => '购买商品',
   'foot_online' => '线上购买',
   'foot_channel' => '发现频道',
   'foot_aws' => '亚马逊',

   'foot_corporation' => '团队',
   'foot_distributor' => '经销商',

   'foot_sub' => '邮箱订阅',
   'foot_sub_pla' => '请输入你的邮箱',
   'foot_sub_message' => '如果你订阅这个邮箱后',
   'foot_sub_message_suc' => '订阅成功',
   'foot_sub_message_error' => '邮箱格式错误!',

   'foot_lang' => '语言',
   'foot_zh' =>'简体中文',
   'foot_en' => '英文',

   'foot_privacy' => '隐私',
   'foot_sales' => '售后',
   // 'foot_careers' => '职业',
   'foot_terms' => '团队的',
   'foot_legal' => '底部legal',

   'foot_select_lang' =>'请选择语言',
   'foot_select_en' => '英文',
   'foot_select_zh' => '中文'

];