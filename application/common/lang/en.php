<?php

return [

    //第一页
   'last_index_p1_title' => '1000 Robots in One',
   'last_index_p1_title1' => 'Build your robot, your way',
   'last_index_p1_watch' => 'Watch video',

   //第二页
   'last_index_p2_title' => 'Your Newest Family Member',
   'last_index_p2_title2' => '1000+robots| 200+ Interactions| 50+uses Cases| Drag-Drop Coding',

   //第三页
   'last_index_p3_title' => 'What is ClicBot?',
   'last_index_p3_content1' => "ClicBot is your family's newest friend. They listen, they think, and even react. ClicBot's personality is contagious, and the playful design makes them even more lovable. More than that, ClicBot is also an engaging teacher that knows how to make learning fun. ",

   'last_index_p3_content2' => 'Create 1000+ different robots with one kit. The discovery threshold will surprise and delight the whole family.',

   //第四页
  'last_index_p4_title' => 'ClicBot Universe',
  'last_index_p4_content1' => 'ClicBot is an entire universe. With more than 1000 different robots, they can climb, dance, crawl, drive, or even serve you your morning coffee.',

  //第五页
  'last_index_p5_title' => 'Face Auto-Tracker',

  //第六页
  'last_index_p6_title' => 'Play Games',



  //第七页
  'last_index_p7_title' => 'Serve coffee',

  //第八页
  'last_index_p8_title' => 'Battle your friends',
  'last_index_p8_buy' => 'Buy',

  //订阅
  'sub_placeholder' => 'Your email address',
  'sub_message_tips' => 'Get the latest news and promotions',
  'sub_subscribe' => 'Subscribe',
  'sub_message' => 'By clicking the button to subscribe, you acknowledge that you have authorized us to send you email, direct mail and customized online advertising. You can unsubscribe at anytime by clicking the link in the footer of our email.',

  'sub_message_success' => 'Subscribe to the success',
  'sub_message_error' => 'Please examine the email address.Thanks!',

  //第九页
  'last_index_p9_title' => 'Build a Robot Companion',
  'last_index_p9_content1' => 'With 200+ interactions, ClicBot allows you to build a truly unique robot companion
ClicBot can play with you',

  //第十页
  'last_index_p10_title' => 'ClicBot can play with you',
  'last_index_p10_content1' => 'An adventurous robot, Bic is a self-balancing two-wheeled ClicBot that loves to roam and explore.',

  //第十一页
  'last_index_p11_title' => 'ClicBot can sense you',
  'last_index_p11_content1' => 'A curious, intellectual soul, Bac responds to touch, sight, and gestures. He enjoys dancing, but gets a bit shy about it.',

  //第十二页
  'last_index_p12_title' => 'Make your own ClicBot',
  'last_index_p12_content1' => 'You can easily build a unique robot by “Clicing” modules together.',
  'last_index_p12_buy' => 'Buy',


  //第十三页
  'last_index_p13_title' => 'Creative Learning',
  'last_index_p13_content1' => 'Teach your kids to code by applying their creativity to make a tangible robot, instead of just copying code snippets.',

  'last_index_p13_title1' => 'Motion Programming',
  'last_index_p13_title2' => 'Learning to Code',

  'last_index_p13_content2' => 'You can assign movement directions by moving your ClicBot and selecting the direction in which its moving. Or you can record a sequence by simply hitting record and moving the bot.',

  'last_index_p13_content3' => 'ClicBot is fully programmable with a friendly drag-and-drop coding interface based on Blockly by Google. With tons of sensors, you can make it do virtually anything.',

  //第十四页
  'last_index_p14_title' => 'Long lasting interest',
  'last_index_p14_content1' => 'ClicBot is constantly updated with new builds, features, and accessories to keep users engaged.',

  //第十五页
  'last_index_p15_title' => 'Everybody matters',
  'last_index_p15_content1' => 'When students love their teachers, amazing things happen.',


  //第十六页
  'last_index_p16_title' => 'Modules and Attachments',

  //Brain
  'last_index_p16_title1' => 'Brain',
  'last_index_p16_content1' => 'The Brain is the master control and power supply unit of ClicBot. Brain has a number of integrated features including an accelerometer, gyroscope, microphone, loudspeaker, camera, and Wi-Fi.',


  //Skeleton
  'last_index_p16_title2' => 'Skeleton',
  'last_index_p16_content2' => 'The Skeleton Module is primarily used for building and extending limbs. Two strip status indicators indicate connection status to other modules.',


  //Grasper
  'last_index_p16_title3' => 'Grasper',
  'last_index_p16_content3' => 'The Grasper is used to pick up and manipulate objects.',

  //Wheel
  'last_index_p16_title4' => 'Wheel',
  'last_index_p16_content4' => 'The Wheel Module is used for vehicle setups. The Wheels include a DC-geared motor and a magnetic speed sensor, with a maximum rotor speed of 4.5 rounds-per-second.',

  //Joint
  'last_index_p16_title5' => 'Joint',
  'last_index_p16_content5' => 'The Joint Module is a high-precision servo system and uses a built-in DC geared motor and built-in angular rate/angle position sensors.',

  //Mount
  'last_index_p16_title6' => 'Mount',
  'last_index_p16_content6' => 'The Mount can be used to affix your ClicBot to tables and other flat surfaces.',

  //Smart Foot
  'last_index_p16_title7' => 'Smart Foot',
  'last_index_p16_content7' => 'The Smart Foot is a functional sensor which can be used to create walking configurations.',


  //Distance Sensor
  'last_index_p16_title8' => 'Distance Sensor',
  'last_index_p16_content8' => 'The Distance Sensor is used to detect objects and in self-navigating configurations.',

  //Suction Cup
  'last_index_p16_title9' => 'Suction Cup',
  'last_index_p16_content9' => 'The Suction Cup is used for climbing and object manipulation. The Suction Cup includes a negative pressure pump and a solenoid valve. It contains an air pressure sensor to monitor suction stability.',

  //Phone Holder
  'last_index_p16_title10' => 'Phone Holder',
  'last_index_p16_content10' => 'The Holder can be used to connect a cellphone or sports camera to your ClicBot. It uses a standard ¼ inch screw attachment.',

  //locker
  'last_index_p16_title11' => 'Locker',
  'last_index_p16_content11' => 'The Locker module is used for reinforcing connections between other modules.',

];