<?php

return [
// 头部导航

   'head_support' => 'Support',
   'head_about_us' => 'About us',
   'head_contact' => 'Contact',
   'head_shopping' => 'Shopping Mall',
   'head_search_place' => 'How can we help you',
   'head_product_faqs' => 'Product FAQs',
   'head_shopping_faqs' => 'Shopping FAQs',
   'head_troubles' => 'Troubleshooting',
   'head_repair_service' => 'Repair service',
   'head_software_upgrade' => 'Software upgrade',
   'head_user_manual' => 'User Manual',

// 底部导航
   'foot_buy' => 'Buy product',
   'foot_online' => 'Buy Online',
   'foot_channel' => 'Find the nearest sales channel',
   'foot_aws' => 'Amazon',

   'foot_corporation' => 'Cooperation',
   'foot_distributor' => 'Distributor',

   'foot_sub' => 'Subscribe our newsletter',
   'foot_sub_pla' => 'Your email address',
   'foot_sub_message' => 'By clicking the button to subscribe, you acknowledge that you have authorized us to send you email, direct mail and customized online advertising. You can unsubscribe at anytime by clicking the link in the footer of our email.',
   'foot_sub_message_suc' => 'Subscribe to the success',
   'foot_sub_message_error' => 'Please examine the email address.Thanks!',

   'foot_lang' => 'Languages',
   'foot_zh' =>'Chinese',
   'foot_en' => 'English',

   'foot_privacy' => 'Privacy Policy',
   'foot_sales' => 'Sales Policy',
   // 'foot_careers' => 'Careers',
   'foot_terms' => 'Terms of Usage',
   'foot_legal' => 'Legal Information',
   'foot_select_lang' => 'Select your language',
   'foot_select_en' => 'English',
   'foot_select_zh' => 'Chinese',  


    // head

    'public_head_support' => 'Support',
    'public_head_about' => 'About us',
    'public_head_contact' => 'Contact', 
    'public_head_contact' => 'BUY',  // 買い
    
    // foot

    // index
    'index_p1_makeit' => 'MAKE IT HAPPEN',
    'index_p1_watch_video' => 'Watch video',
    'index_p1_easy' => 'Easy to build setups within minutes - advanced modular technology gives you full creative control with our easy "clic" technology. Big Ideas start with a CLIC!',

    'index_p2_alive' => "It's alive!",
    'index_p2_right' => "That's Right!",
    'index_p2_clicbot' => "ClicBot comes with hundreds of unique interactions built in and will react to your movements and touch - it's so life like!",

    'index_p4_want' => 'Want ClicBot to walk?',
    'index_p4_how'  => 'How about making ClicBot climb?',
    'index_p4_what' => 'What about making ClicBot dance?',
    'index_p4_its' => "It's all possible with ClicBot and so much more!",
    'index_p4_just' => 'Just build, program, and play!',

    'index_p5_when' => 'When students love their teacher, amazing things happen',
    'index_p5_want' => 'Want to build a ClicBot?',
    'index_p5_clicbot' => 'ClicBot is a',
    'index_p5_clicbot1' => ' modular robot',
    'index_p5_clicbot2' => ', which means that it can be ',
    'index_p5_clicbot3' => 'built and rebuilt',
    'index_p5_clicbot4' => ' in many ways using different modules and combinations! With the Brain, joints, skeleton, mount, or many other modular pieces, you can easily build a ClicBot that is just one of thousands of set-ups. What type of robot you build is entirely up to you!',

    'index_p5_just' => 'Just click them together!',
    'index_p5_clic' => 'ClicBot',
    'index_p5_meet' => 'Meet Bac and Bic',
    'index_p5_new' => 'new friends',
    'index_p5_timid' => 'Timid',
    'index_p5_dance' => 'Dancer',
    'index_p5_scientist' => 'Scientist',
    'index_p5_mobile' => 'Mobile',
    'index_p5_selef' => 'Self-driving',
    'index_p5_adv' => 'adventrous',
    
    'index_p6_exc' => 'Exciting Vehicles',
    
    'index_p6_inter' => 'Interested in having a car racing competition with your friends? Use ClicBot to build your own unique speed racer for your racecourses!',
    
    'index_p6_be' => 'Be a Mighty Explorer',
    
    'index_p6_clicbot' => 'ClicBot can explore the world around you! ClicBot can walk, run, drive, or even climb on windows!',
    
    'index_p6_cutting' => 'Cutting-edge innovation',
    
    'index_p6_clic' => 'ClicBot is on the cutting-edge of robotics technology. Check out Bic - our two wheeled self-balancing robot that can roam around with you on your great adventures.',
    
    'index_p6_mobile' => 'Mobile Personal Assistant',
    
    'index_p6_film' => "Film on the go with this phone-holding car! ClicBot can be your little helper when you'd like to take cool photos & videos. What's more, it can run with you!",
    
    'index_p6_power' => 'Powerful Modules for special builds',
    
    'index_p6_cust' => 'Customize ClicBot to perform personalized actions. Try setting up a distance sensor to get a warm greeting next time you meet your ClicBot!',
    
    'index_p6_a' => 'A Partner for Family Activities',
    
    'index_p6_aperfect' => 'A perfect addition to family fun, whether a buddy to cozy up to, a friend to play with, or even just a companion to share a good book.',
    
    'index_p6_design' => 'Design for the task',
    
    'index_p6_cars' => 'Cars are fun, but how about an intelligent one! Build your ClicBot for the task at hand, whether grasping objects, crossing bridges, or even navigating the environment around you!',

    'index_p6_indoors' => 'Indoors and outdoors',

    'index_p6_clicbotis' => 'ClicBot is an adventurer! Whether indoors or outdoors, you can design the perfect robot to explore the world together.',

    'index_p6_fun' => 'Fun for the whole family',

    'index_p6_play' => 'Play exciting games with the whole family! ClicBot is a perfect choice to bring everyone together and showcase your amazing ideas!',
    
    'index_p6_your' => 'Your best robot friend',

    'index_p6_with' => "With a big personality, ClicBot may just become your new best friend! Share birthdays, playtime, and get-togethers with ClicBot and friends, it's sure to impress!",

    'index_p6_inter' => 'Interactive games & features',

    'index_p6_discover' => "Discover ClicBot's multiple games, interactions, and features! Whether it's a fun dance, a game of trivia, or even playful reactions, ClicBot is great entertainment.",

    'index_p6_education' => 'Educational growth through hands-on learning',

    'index_p6_learn' => 'Learn how to code with ClicBot and understand how robotics works and operates through fun and interactive features that lets YOU make what you want.',

    'index_p6_explore' => 'Explore and express your ideas',

    'index_p6_reima' => 'Reimagine your world with powerful modules, like the wall climbing Suction Cup! Modules help you create more than just interesting designs, but also explore the world of robotics through innovative thought and imagination.',
    
    'index_p6_exper' => 'Experiment with modular robotics',

    'index_p6_hands' => 'Hands-on learning and exploring is a great way to learn and express your inner creativity. Build a ClicBot with your friends, share ideas, and come up with something entirely new!',
    

    'index_p7_idea' => 'Your Idea, Your ClicBot',
    'index_p7_modules' => 'Modules that let you make ClicBot your own',
    'index_p7_discover' => 'Discover more by clicking modules',

    'index_p8_introduction' => 'Introducing Demo Motion',
    'index_p8_the' => 'The easiest way to make your coolest friend',
    'index_p8_with' => 'With Demo Motion, programming a robot is easier than ever! Just move ClicBot in the motions you want it to perform, and it can remember and complete the entire process smoothly. You only need to consider what you want to do, and ClicBot will do the rest!',
    
    'index_p9_drag' => 'Drag & Drop graphical programming',
    'index_p9_look' => 'Looking for something a little more advanced? Our user-friendly drag & drop programming tool allows you to connect preconfigured ClicBot actions together.',

    'index_p10_clicbot' => 'ClicBot Community',
    'index_p10_share' => 'Share the magic',
    'index_p10_community' => 'Community is a place where all ClicBoters can share their interesting creations! Made something magical that you want to share with the world? Simply upload your artworks, photos, code programs, or ClicBot set-ups for others to download and try out for themselves.If you are looking for inspiration, community is a fantastic hub for discovering even more uses for your ClicBot. Share the magic, share the fun!',


    // about us 

    'aboutt_title' => 'Corporation Introduction',

    'aboutt_content1' => 'Founded in 2014, KEYi Tech is an innovative robotics company with expertise in designing and developing STEAM Educational Robots (Science, Technology, Engineering, Arts, and Mathematics). Our team consists of top engineers and designers with previous experience from Samsung and Intel, with academic backgrounds from top universities worldwide. We own more than 40 independent intellectual properties ranging from robotic designs to AI algorithms.',

     'about_content2' => 'Our R&D department spent nearly 2 years working on ClicBot before showcasing at the 2020 CES conference in Las Vegas, reported by top media outlets like Forbes, Mashable, and TechCrunch. ClicBot has 50+ predefined use cases, 200+ reactions, and over 1000+ setups. Using our industrial level algorithms, ClicBot has smoother and more natural movements than other robots, giving it personality and making it a real companion.',
   
   'about_team' => 'Lead Team Introduction',

 


];