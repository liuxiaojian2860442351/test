<?php

namespace app\api\controller\v1;

use think\Controller;
use think\Request;
use think\Db;

class Index extends Common
{


    public function index()
    {
        //首页的信息,版本1的的接口信息

        $data = [
            "user_id" => "89",
            "nick_name" => "liujian",
        ];
        $result = ["code" => "200", "msg" => "请求成功", "data" => $data];
        return json_encode($result);
    }

    public function getUserInfo($id = "")
    {
        //接口的获取
        //根据内容的id来返回内容的基本信息
        $data = array();
        if (empty($id) || $id == "") {
            $data = Db::table('test')->select();
        } else {
            $data = Db::table('test')->where("id", $id)->select();
        }
        if (count($data) >= 1) {
            $result = ["code" => "200", "msg" => "请求成功", "data" => $data];
        } else {
            $result = ["code" => "500", "msg" => "请求失败", "data" => $data];
        }
        return json_encode($result);
    }

    public function submit($content, $des)
    {
        //传入两个参数，一个是内容，一个是描述,数据的增加
        if (!empty($content)) {
            $data = ['content' => $content, 'des' => $des];
            $rows = Db::table('test')->insert($data);
            $newdata = Db::table('test')->select();
            if ($rows >= 1) {
                $result = ["code" => "200", "msg" => "上传成功", "data" => $newdata];
            } else {
                $result = ["code" => "500", "msg" => "上传失败", "data" => $newdata];
            }
        } else {
            $result = ["code" => "501", "msg" => "评论的内容不得为空", "data" => []];
        }

        return json_encode($result);
    }

    public function delete($id)
    {
        // 数据的删除,软删除,将数据的status的状态置为1
        //status=1表示数据已经被删除
        if (!empty($id)) {
            $rows = Db::table('test')->where('id', $id)->update(['status' => '1']);
            $data = Db::table('test')->where('id', $id)->select();
            if ($rows >= 1) {
                $result = ["code" => "200", "msg" => "删除成功", "data" => $data];
            } else {
                if ($data[0]['status'] == 1) {
                    $result = ["code" => "500", "msg" => "删除失败,该条数据已经被删除，不可以重复删除", "data" => []];
                } else {
                    $result = ["code" => "500", "msg" => "删除失败", "data" => []];
                }
            }
        } else {
            $result = ["code" => "501", "msg" => "删除失败,请传入需要删除的用户名称", "data" => []];

        }
        return json_encode($result);


    }

    public function update($id, $content, $des)
    {
        $old_des = Db::table('test')->field('des')->where('id', $id)->select();
        //获取历史的描述字段
        $old_des = $old_des[0]['des'];
        if (empty($des) || $old_des == $des) {
            $des = $old_des;
        }
        $data = ['content' => $content, 'des' => $des];
        $rows = Db::table('test')->where('id', $id)->update($data);
        if ($rows >= 1) {
            //根据id来查询数据
            $data = Db::table('test')->where('id', $id)->select();
            $result = ["code" => "200", "msg" => "修改成功", "data" => $data];
        } else {
            //根据id来查询数据
            $data = Db::table('test')->where('id', $id)->select();
            $result = ["code" => "500", "msg" => "修改失败", "data" => $data];

        }
        return json_encode($result);

    }

}