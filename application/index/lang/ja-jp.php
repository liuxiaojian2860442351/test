<?php

return [
// 头部导航

   'head_support' => 'サポート',
   'head_about_us' => '私たちについて',
   'head_contact' => '連絡',
   'head_shopping' => 'Shopping Mall',
   'head_search_place' => 'How can we help you',
   'head_product_faqs' => 'Product FAQs',
   'head_shopping_faqs' => 'Shopping FAQs',
   'head_troubles' => 'Troubleshooting',
   'head_repair_service' => 'Repair service',
   'head_software_upgrade' => 'Software upgrade',
   'head_user_manual' => 'User Manual',

// 底部导航
   'foot_buy' => 'Buy product',
   'foot_online' => 'Buy Online',
   'foot_channel' => 'Find the nearest sales channel',
   'foot_aws' => 'Amazon',

   'foot_corporation' => 'Cooperation',
   'foot_distributor' => 'Distributor',

   'foot_sub' => 'Subscribe our newsletter',
   'foot_sub_pla' => 'Your email address',
   'foot_sub_message' => 'By clicking the button to subscribe, you acknowledge that you have authorized us to send you email, direct mail and customized online advertising. You can unsubscribe at anytime by clicking the link in the footer of our email.',
   'foot_sub_message_suc' => 'Subscribe to the success',
   'foot_sub_message_error' => 'Please examine the email address.Thanks!',

   'foot_lang' => 'Languages',
   'foot_zh' =>'Chinese',
   'foot_en' => 'English',

   'foot_privacy' => 'Privacy Policy',
   'foot_sales' => 'Sales Policy',
   // 'foot_careers' => 'Careers',
   'foot_terms' => 'Terms of Usage',
   'foot_legal' => 'Legal Information',
   'foot_select_lang' => 'Select your language',
   'foot_select_en' => 'English',
   'foot_select_zh' => 'Chinese',  


    // head

    'public_head_support' => 'サポート',
    'public_head_about' => '私たちについて',
    'public_head_contact' => '連絡', 
    'public_head_referral' => '', 
    'public_head_buy' => 'BUY',  // 買い
    
    // foot

    // index
    'index_p1_makeit' => '遊びを通して学ぼう',
    'index_p1_watch_video' => '動画を見る',
    'index_p1_easy' => '数分で簡単にセットアップができる - 先進のモジュラー技術により、私達が生みだした簡単な「クリック」技術でクリエイティブな操作が可能になりました。クリックして、あなたの冒険が始まります!',
    'index_p1_makein' => 'ClicBotと一緒にあなたのアイデアを実現しましょう!',
    'index_p1_intelligent' => 'ClicBotはすべての年齢層のために設計<span>された高度な知的</span>コーディングロボットです',
    'index_p1_steam' => 'ClicBotがあれば、STEM学習がもっと簡単',
    'index_p1_the' => 'に、もっと面白くなります',

    'index_p2_alive' => "生きてる！",
    'index_p2_right' => "そうです。",
    'index_p2_clicbot' => "ClicBotには何百ものユニークなインタラクションが内蔵されており、動いたりタッチしたりすると反応します。",
    'index_p2_modular' => 'モジュラー',
    'index_p2_coding' => 'コーディング',
    'index_p2_robot' => '知的ロボット',

    'index_p2_modular' => 'モジュラー',
    'index_p2_coding' => 'コーディング',
    'index_p2_robot' => '知的ロボット',
    
    'index_p3_is' => 'が誕生しました！',

    'index_p4_want' => 'ClicBotに歩かせたい？',
    'index_p4_how'  => '登らせるのはどうですか？',
    'index_p4_what' => 'ClicBotに踊らせてみませんか？',
    'index_p4_its' => "ClicBotの可能性は無限大。",
    'index_p4_just' => '作って、プログラムして、遊ぶだけ!',

    'index_p5_when' => '生徒が先生を好きになれば、驚くようなことが起こる',
    'index_p5_want' => 'ClicBotを組み立ててみませんか？',
    'index_p5_clicbot' => 'ClicBotは',
    'index_p5_clicbot1' => ' モジュール型ロボットで、',
    'index_p5_clicbot2' => '様々なモジュールや組み合わせを使って',
    'index_p5_clicbot3' => '、色々な方法で組み立てたり',
    'index_p5_clicbot4' => '、再構築したりすることができます! ブレイン、ジョイント、スケルトン、マウント、他たくさんのモジュラーピースを使用して、世界で１つだけのClicBotを簡単に構築することができます。どんなロボットを作るかはあなた次第です！',

    'index_p5_just' => '一緒にクリックす',
    'index_p5_clic' => 'るだけ!',
    'index_p5_meet' => 'BacとBicの出会い',
    'index_p5_new' => '新しい友達',
    'index_p5_timid' => '臆病な',
    'index_p5_dance' => 'ダンサーであり',
    'index_p5_scientist' => '科学者',
    'index_p5_mobile' => '動き回る',
    'index_p5_selef' => '自動運転の',
    'index_p5_adv' => '冒険家',
    
    'index_p5_your' => 'あなたの想像力をみせてください！',
    'index_p5_creat' => '創造性は可能性につながる',
    'index_p5_creativity' => '創造性は教育の中核をなすものです。だからこそ、STEM学習が近年ますます人気を博しているのです。あなたがより創造的であればあるほど、将来的により多くの可能性を秘めています。ClicBotは、STEM学習への興味を活性化させるように設計されており、多くのセットアップがあるので飽きることがありません。',


    'index_p6_exc' => 'エキサイティングな乗り物',
    
    'index_p6_inter' => '友達と一緒にレース大会をしてみませんか？ClicBotを使って、あなただけのスピードレーサーを作ってみませんか？',
    
    'index_p6_be' => '並外れた探検家になろう！',
    
    'index_p6_clicbot' => 'ClicBotはあなたと共に世界を探索することができます! ClicBotは歩いたり、走ったり、運転したり、窓を這ったりすることができます。',
    
    'index_p6_cutting' => '最先端のイノベーション',
    
    'index_p6_clic' => 'ClicBotはロボット技術の最先端を走っています。Bicをチェックしてみてください - 自らバランスのとれる二輪ロボットがあなたの生活のパートナーになってくれます。',
    
    'index_p6_mobile' => 'あなたのパーソナルアシスタントに',
    
    'index_p6_film' => "この電話を持つ車で撮影しよう! ClicBotは、クールな写真やビデオを撮りたいときの小さな助っ人になります。さらには、あなたと一緒に走ることができます。",
    
    'index_p6_power' => '特別な組み立てのための強力なモジュール',
    
    'index_p6_cust' => 'ClicBotのアクションを思うままにカスタマイズできます。距離センサーを設定して、ClicBotに会った時に挨拶をしてみましょう。',
    
    'index_p6_a' => '家族の活動のパートナー',
    
    'index_p6_aperfect' => '家族の楽しみに加えて、居心地の良いバディ、一緒に遊ぶ友達、あるいは良い本を共有する仲間にもなってくれます。',
    
    'index_p6_design' => 'タスクのためのデザイン',
    
    'index_p6_cars' => '車も楽しいけど、知的なものはどうでしょう？物体をつかんだり、橋を渡ったり、周りの環境をナビゲートしたりと、目の前のタスクのためにClicBotを作りましょう!',

    'index_p6_indoors' => '屋内でも屋外でも',

    'index_p6_clicbotis' => 'ClicBotは冒険家です! 屋内でも屋外でも、一緒に世界を冒険するための完璧なロボットをデザインすることができます。',

    'index_p6_fun' => '家族みんなで楽しめる',

    'index_p6_play' => '家族全員でエキサイティングなゲームをプレイしましょう！ ClicBotはみんなで集まって、あなたの素晴らしいアイデアを披露するのに最適です!',
    
    'index_p6_your' => 'あなたの最高のパートナーに',

    'index_p6_with' => "ClicBotはあなたの新しい親友になるかもしれません。誕生日や遊びの時間を共有したり、ClicBotや友達と一緒に集まり、感動すること間違いなし!",

    'index_p6_inter' => 'インタラクティブなゲームと機能',

    'index_p6_discover' => "ClicBot の複数のゲーム、インタラクション、機能を発見してください! 楽しいダンスでも、豆知識ゲームでも、遊び心のあるリアクションでも、ClicBotは最高のエンターテイメントです。",

    'index_p6_education' => '体験学習による教育的成長',

    'index_p6_learn' => 'ClicBotを使ったコーディング方法を学び、ロボット工学がどのように機能し、どのように動作するのかを理解し、楽しくインタラクティブな機能を使って自分の欲しいものを作ることができます。',

    'index_p6_explore' => 'あなたのアイデアを探求し、表現する',

    'index_p6_reima' => '壁登りのサクションカップのような強力なモジュールで、あなたの世界を再考してみましょう! モジュールは、単に面白いデザインを作るだけでなく、革新的な思考と想像力でロボット工学の世界を探求するのに役立ちます。',
    
    'index_p6_exper' => 'モジュラーロボットで実験',

    'index_p6_hands' => '実践的な学習と探索は、あなたの内なる創造性を学び、表現するのに最適な方法です。友達と一緒にClicBotを作って、アイデアを共有して、全く新しいものを作りましょう。',
    

    'index_p6_and' => 'まだまだあります！',
    'index_p6_mobile_per' => '',
    'index_p6_film_on' => '',
    'index_p6_hands_on' => '実践学習',
    'index_p6_education_through' => '体験学習を通じた教育は、STEM学習に大きな影響を与え、面白くしてくれます。あなたのアイデアや想像力に合わせてパーツをクリックしていくだけで、あなたの想像力は無限大であることに気づくでしょう。',
    'index_p6_program' => 'プログラム',
    'index_p6_with_clicbot' => 'ClicBot アプリでは、あなたが望むものを作ることができます！楽しいインタラクティブな機能を通じて、ロボットがどのようにするかを理解でき、またコード化する方法を学ぶことができます。',
    'index_p6_take' => '写真を撮る',
    'index_p6_using' => 'ClicBotを使って、スケート、冬のスキー、春の旅行など、あなたの思い出の瞬間を記録することができます。',
    'index_p6_climb' => '窓の上に登る',
    'index_p6_spider' => 'スパイダーマンってかっこいいですよね。サクションカップを使えば、窓を這うことができるロボットを作ることができます。',
    'index_p6_grasp' => '物をつかむ',
    'index_p6_its' => "物体をつかむことができるロボットカーを作ってみませんか？様々な形の物体を掴むことができるグラスパーパーツは、遊びの中でたくさんの楽しみをもたらしてくれるでしょう。",
    'index_p6_play_trivia' => 'クイズで遊ぶ',
    'index_p6_youcan' => '組み立てたり、Bacと交流したりするだけでなく、クイズゲームをすることもできます。手を右か左にかざして答えを選択し、結果が画面に表示されます。',   
    'index_p6_build' => '歩くロボットを作る',
    'index_p6_with_sub' => 'サクションカップを使えば、スパイダーロボットを作るだけでなく、二足歩行のロボットを作ることもできます。',
    'index_p6_phone' => 'スマホホルダー',
    'index_p6_clicbot_is' => 'ClicBotはロボットであり、生活の中であなたの小さなヘルパーになることもできます。写真を撮りたいときは、スマホホルダーを使ってヘルパーロボットを作るだけです。',
    'index_p6_two' => '自動二輪車のセルフバランスカー',
    'index_p6_how' => 'Bicでレースをしてみませんか？独自の二輪セルフバランスカー。1つの脳、1つのジョイント、2つの車輪を使用するだけで、あなたは二輪のセルフバランスカーを作成できます。',
    'index_p6_car' => 'カーレース',
    'index_p6_want' => 'あなたの友人や家族と一緒にカーレースをしませんか？あなたは一緒にホイールを使用して異なるモジュールをクリックして、非常に多くのかっこいい車を作ることができます。',
    'index_p6_dance' => 'Bacとダンス',
    'index_p6_bac' => 'Bacはダンスが大好き! 可愛くてやんちゃな性格のロボット「バック」と一緒に踊ってみませんか？ユニークでおもしろいダンスタイムが楽しめること間違いなしです。',
    'index_p6_play_with' => 'Bicで遊ぶ',
    'index_p6_two_wheel' => '二輪のセルフバランスロボット - 「ビック」はたった2つの車輪であなたの小さな世界を旅することができます! 四輪車もいいけど、二輪車もかっこいい…！？',
    'index_p6_dancing' => 'ダンシングカー',
    'index_p6_building' => '見ることができ、笑うことができ、そして踊ることもできる車を作ることができます。もちろん、走ることもできます:)',




    'index_p7_idea' => 'あなたのアイデア、あなたのClicBot',
    'index_p7_modules' => 'ClicBotを自分のものにするためのモジュール',
    'index_p7_discover' => '',
    'index_p7_click' => 'Click Here!',

    'index_p8_introduction' => 'デモモーションの紹介',
    'index_p8_the' => '一番簡単にクールな友達を作る方法',
    'index_p8_with' => 'Demo Motionを使えば、ロボットのプログラミングがこれまで以上に簡単になります。ClicBotを好きなように動かすだけで、その動きをスムーズに覚えてくれます。あなたはやりたいことを考えるだけで、あとはClicBotが覚えてくれます。',
    
    'index_p9_drag' => 'ドラッグ＆ドロップによるグラフィカルなプログラミング',
    'index_p9_look' => 'もう少し高度なレベルをお探しですか？私たちのユーザーフレンドリーなドラッグ＆ドロッププログラミングツールで、あらかじめ設定されたClicBotアクションを一緒に接続することができます。',

    'index_p10_clicbot' => 'ClicBot コミュニティ',
    'index_p10_share' => '魔法を共有しましょう',
    'index_p10_community' => 'コミュニティは、すべてのClicBotユーザーが面白い作品を共有できる場所です。何かマジックのようなものを作って、世界と共有したいですか？あなたの作品、写真、コードプログラム、ClicBotのセットアップをアップロードするだけで、他のユーザーがダウンロードして試してみることができます。みんなで楽しさを共有しましょう。',

     
     'index_p11_create' => '自分だけのロボットを作りたいですか？簡単です！',
     'index_p11_with_demo' => 'デモモーションを使えば、ロボットのプログラミングが今まで以上に簡単になります。ClicBotを動かしたい動作に合わせて動かし、アプリに覚えさせるだけで、あなただけのロボットが完成。やりたいことを考えて、ClicBotで実現させましょう。',

     'index_p11_go' => '初心者から上級者まで！',
     'index_p11_look' => 'もう少し高度なものをお探しですか？初心者向けのドラッグ＆ドロップ式のプログラミングツールなら、あらかじめ設定されたClicBotのアクションをつなぎ合わせることができます。コーディングの知識がなくても、簡単にClicBotをプログラミングすることができます。',

     'index_p11_share' => '魔法と楽しさを共有しよう！',
     'index_p11_community' =>'ClicBot アプリのコミュニティは、すべてのClicBoterが自分の面白い作品を共有できる場所です。何か魔法のようなものを作って、世界と共有しませんか？あなたの作品、写真、コードプログラム、ClicBotのセットアップをClicBotコミュニティにアップロードしてください。インスピレーションを求めているなら、コミュニティは素晴らしい場所です。他の人が作成したセットアップを見たり、ダウンロードしたり、自分で試してみたりすることができます。',

     'index_p12_please' => 'ニュースレターで在庫の更新や最新ニュース＆プロモーションを発信しています！',

     'index_p12_your' => 'あなたのメールアドレス',
     'index_p12_sub' => '登録する',
    // about us
    'about_title' => '会社紹介',

    'about_content1' => '2014年に設立されたKEYi Techは、STEAM教育用ロボット（Science, Technology, Engineering, Arts, and Mathematics）の設計・開発を専門とする革新的なロボット企業です。当社のチームは、サムスンやインテルでの前職経験を持ち、世界中のトップ大学での学歴を持つトップエンジニアやデザイナーで構成されています。ロボット設計からAIアルゴリズムまで、40以上の独立した知的財産を所有しています。',
    'about_content2' => '当社の研究開発部門は約2年をかけてClicBotを開発し、2020年にラスベガスで開催されるCESカンファレンスで発表し、フォーブス、マッシャブル、テッククランチなどのトップメディアにも報道されました。ClicBotには50以上の事前定義されたユースケース、200以上の反応、1000以上のセットアップがあります。当社の産業レベルのアルゴリズムを使用したClicBotは、他のロボットよりも滑らかで自然な動きをしており、個性を与え、本当の仲間にしてくれます。',
   
   'about_team' => 'リードチームの紹介',

   'index_foot_email1' => 'ニュースレターで在庫の更新や',
   'index_foot_email2' => '最新ニュース、プロモーションを',
   'index_foot_email3' => '発信しています！',
   'index_foot_email' => 'sales3@emi-trading.biz',


];