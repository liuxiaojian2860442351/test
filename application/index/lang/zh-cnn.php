<?php

return [
// 头部导航

   'head_support' => '产品支持',
   'head_about_us' => '关于我们',
   'head_contact' => '联系我们',
   'head_shopping' => 'Shopping Mall',
   'head_search_place' => 'How can we help you',
   'head_product_faqs' => 'Product FAQs',
   'head_shopping_faqs' => 'Shopping FAQs',
   'head_troubles' => 'Troubleshooting',
   'head_repair_service' => 'Repair service',
   'head_software_upgrade' => 'Software upgrade',
   'head_user_manual' => 'User Manual',

// 底部导航
   'foot_buy' => 'Buy product',
   'foot_online' => 'Buy Online',
   'foot_channel' => 'Find the nearest sales channel',
   'foot_aws' => 'Amazon',

   'foot_corporation' => 'Cooperation',
   'foot_distributor' => 'Distributor',

   'foot_sub' => 'Subscribe our newsletter',
   'foot_sub_pla' => 'Your email address',
   'foot_sub_message' => 'By clicking the button to subscribe, you acknowledge that you have authorized us to send you email, direct mail and customized online advertising. You can unsubscribe at anytime by clicking the link in the footer of our email.',
   'foot_sub_message_suc' => 'Subscribe to the success',
   'foot_sub_message_error' => 'Please examine the email address. Thanks!',

   'foot_lang' => 'Languages',
   'foot_zh' =>'Chinese',
   'foot_en' => 'English',

   'foot_privacy' => 'Privacy Policy',
   'foot_sales' => 'Sales Policy',
   // 'foot_careers' => 'Careers',
   'foot_terms' => 'Terms of Usage',
   'foot_legal' => 'Legal Information',
   'foot_select_lang' => 'Select your language',
   'foot_select_en' => 'English',
   'foot_select_zh' => 'Chinese',  


    // head

    'public_head_support' => '产品支持',
    'public_head_about' => '关于我们',
    'public_head_contact' => '联系我们', 
    'public_head_referral' => '',     
    'public_head_buy' => 'BUY',  // 買い
    
    // foot

    // index
    'index_p1_makeit' => 'MAKE IT HAPPEN',
    'index_p1_watch_video' => '观看视频',
    'index_p1_easy' => '分分钟搭建好你的机器人——前沿模块化科技带给你全新体验。大创意始于小主意！',
    'index_p1_makein' => '跟ClicBot⼀起⻅证神奇吧！',
    'index_p1_intelligent' => 'ClicBot是适⽤于所有年龄段的⾼端智能编程机器⼈！',
    'index_p1_steam' => '在ClicBot的陪伴下，STEM教育变得轻松有趣！',
    'index_p1_the' => '',

    'index_p2_alive' => "有生命的机器人伙伴",
    'index_p2_right' => "是的，没错！",
    'index_p2_clicbot' => "ClicBot内置百余种独特的感应功能，与之互动，其会有不同的反应——宛如有生命一般！",
    'index_p2_modular' => '模块化',
    'index_p2_coding' => '编程',
    'index_p2_robot' => '智能机器⼈',
    
    'index_p3_is' => '来啦！！！',


    'index_p4_want' => '想拥有⼀个深谙⽆⼈驾驶的汽⻋机器⼈吗？',
    'index_p4_how'  => '想拥有⼀个知晓⻜檐⾛壁的蜘蛛侠机器⼈吗？',
    'index_p4_what' => '想拥有⼀个会⽤⼿偷你的⼩饼⼲的机器⼈吗？',
    'index_p4_its' => "ClicBot让这⼀切皆有可能！⽽且，还不⽌于这些！",
    'index_p4_just' => '你需要做的仅仅是拼接，编程，玩耍！',

    'index_p5_when' => 'When students love their teacher, amazing things happen',
    'index_p5_want' => '想拼⼀个ClicBot吗？',
    'index_p5_clicbot' => 'ClicBot是模块化机器⼈，也就是说我们可以⽤不同的',
    'index_p5_clicbot1' => ' 模块、不同的⽅式、不断拼接出不同的构型。',
    'index_p5_clicbot2' => '搭建⼀个机器⼈⽐你想象的简单得多，仅仅需要把模',
    'index_p5_clicbot3' => '块拼在⼀起！想要什么样的机器⼈完全是你说了算！',
    'index_p5_clicbot4' => ' ',

    'index_p5_just' => '仅需把模块拼',
    'index_p5_clic' => '在⼀起!',
    'index_p5_meet' => '遇⻅你的两个',
    'index_p5_new' => '新朋友',
    'index_p5_timid' => '害羞',
    'index_p5_dance' => '爱跳舞',
    'index_p5_scientist' => '科学家',
    'index_p5_mobile' => '移动',
    'index_p5_selef' => '⾃驱',
    'index_p5_adv' => '冒险家',
    
    'index_p5_your' => '想象不设限',
    'index_p5_creat' => '创造力引领可能性',
    'index_p5_creativity' => '创造⼒是教育的核⼼，这也是为什么STEM教育越来越受欢迎的原因。越是有创造⼒的孩⼦，在未来越是拥有更多的可能性。ClicBot为激发STEM学习兴趣⽽⽣，其百变构型使你永远不会感到厌倦。',


    'index_p6_exc' => 'Exciting Vehicles',
    
    'index_p6_inter' => 'Interested in having a car racing competition with your friends? Use ClicBot to build your own unique speed racer for your racecourses!',
    
    'index_p6_be' => 'Be a Mighty Explorer',
    
    'index_p6_clicbot' => 'ClicBot can explore the world around you! ClicBot can walk, run, drive, or even climb on windows!',
    
    'index_p6_cutting' => 'Cutting-edge innovation',
    
    'index_p6_clic' => 'ClicBot is on the cutting-edge of robotics technology. Check out Bic - our two wheeled self-balancing robot that can roam around with you on your great adventures.',
    
    'index_p6_mobile' => 'Mobile Personal Assistant',
    
    'index_p6_film' => "Film on the go with this phone-holding car! ClicBot can be your little helper when you'd like to take cool photos & videos. What's more, it can run with you!",
    
    'index_p6_power' => 'Powerful Modules for special builds',
    
    'index_p6_cust' => 'Customize ClicBot to perform personalized actions. Try setting up a distance sensor to get a warm greeting next time you meet your ClicBot!',
    
    'index_p6_a' => 'A Partner for Family Activities',
    
    'index_p6_aperfect' => 'A perfect addition to family fun, whether a buddy to cozy up to, a friend to play with, or even just a companion to share a good book.',
    
    'index_p6_design' => 'Design for the task',
    
    'index_p6_cars' => 'Cars are fun, but how about an intelligent one! Build your ClicBot for the task at hand, whether grasping objects, crossing bridges, or even navigating the environment around you!',

    'index_p6_indoors' => 'Indoors and outdoors',

    'index_p6_clicbotis' => 'ClicBot is an adventurer! Whether indoors or outdoors, you can design the perfect robot to explore the world together.',

    'index_p6_fun' => 'Fun for the whole family',

    'index_p6_play' => 'Play exciting games with the whole family! ClicBot is a perfect choice to bring everyone together and showcase your amazing ideas!',
    
    'index_p6_your' => 'Your best robot friend',

    'index_p6_with' => "With a big personality, ClicBot may just become your new best friend! Share birthdays, playtime, and get-togethers with ClicBot and friends, it's sure to impress!",

    'index_p6_inter' => 'Interactive games & features',

    'index_p6_discover' => "Discover ClicBot's multiple games, interactions, and features! Whether it's a fun dance, a game of trivia, or even playful reactions, ClicBot is great entertainment.",

    'index_p6_education' => 'Educational growth through hands-on learning',

    'index_p6_learn' => 'Learn how to code with ClicBot and understand how robotics works and operates through fun and interactive features that lets YOU make what you want.',

    'index_p6_explore' => 'Explore and express your ideas',

    'index_p6_reima' => 'Reimagine your world with powerful modules, like the wall climbing Suction Cup! Modules help you create more than just interesting designs, but also explore the world of robotics through innovative thought and imagination.',
    
    'index_p6_exper' => 'Experiment with modular robotics',

    'index_p6_hands' => 'Hands-on learning and exploring is a great way to learn and express your inner creativity. Build a ClicBot with your friends, share ideas, and come up with something entirely new!',
    
    'index_p6_and' => '和许多其他朋友！',
    'index_p6_mobile_per' => '',
    'index_p6_film_on' => '',
    'index_p6_hands_on' => '动⼿学习',
    'index_p6_education_through' => '通过动⼿尝试来增进STEM学习的兴趣。仅需按照你的想法，将模块拼接在⼀起，便可实现你想要的构型。你看，想象力从来没有尽头。',
    'index_p6_program' => '编程',
    'index_p6_with_clicbot' => '通过ClicBot APP，你可以学习如何编程，明白机器人的工作原理，也可以做出很多有趣的可以互动的功能。',
    'index_p6_take' => '拍照',
    'index_p6_using' => '用ClicBot，你不仅可以搭建常规云台，而且可以搭建跟随云台。无论你是在冬天滑冰滑雪，还是在春日里旅行，ClicBot跟随云台可以跟着你走并且拍下难忘的时刻。',
    'index_p6_climb' => '爬窗户',
    'index_p6_spider' => '蜘蛛侠很酷对不对？装上Suction Cup模块，⼈⼈都能轻松搭建爬墙机器⼈。',
   'index_p6_grasp' => '抓取物品',
    'index_p6_its' => "做一个会跑的机器人小车很酷，做一个会抓东西的机器人小车是不是更酷？Grasper模块可以抓取形状各异的物体。",
    'index_p6_play_trivia' => '快问快答',
    'index_p6_youcan' => '你不仅可以搭建Bac，和Bac互动，还可以玩“快问快答”智⼒游戏。⽤⼿向右⽐划或者向左⽐划来给出你的答案吧，结果会⽴即出现在屏幕上哦。',   
    'index_p6_build' => '走路机器人',
    'index_p6_with_sub' => '用Suction Cup，你不仅可以做蜘蛛侠机器人，而且可以搭建走路机器人。',
    'index_p6_phone' => '手机支架',
    'index_p6_clicbot_is' => 'ClicBot是机器人，也是你生活中的小助手。当你想拍照的时候，用Phone Holder模块来搭建一个云台机器人吧。',
    'index_p6_two' => '二轮自平衡小车',
    'index_p6_how' => '和Bic来个赛跑怎么样？Bic是我们的二轮自平衡小车。仅仅用1个Brain，1个Joint，2个Wheel，你就可以得到一个二轮自平衡小车。它可以跑得很快哦！',
    'index_p6_car' => '赛车',
    'index_p6_want' => '想和家人朋友一起玩赛车吗？将Wheel模块和其他不同的模块拼接在一起，你可以做出来各种酷酷的车哦。',
    'index_p6_dance' => '与Bac共舞',
    'index_p6_bac' => 'Bac喜欢跳舞！为什么不来和Bac一起跳舞呢？Bac是一个害羞又可爱的机器人。我们相信跟你跟Bac一起跳舞一定能度过一段有趣又独特的时光。',
    'index_p6_play_with' => '与Bic玩耍',
    'index_p6_two_wheel' => '二轮自平衡机器人小车——Bic，用两个轮子就能在你的小小世界中穿行！四轮车很厉害，二轮车是不是更厉害！',
    'index_p6_dancing' => '舞蹈车',
    'index_p6_building' => '做一个会看、会笑、会跳舞的小车吧！别忘了，这个小车还会跑。',

    'index_p7_idea' => '创意使你的ClicBot与众不同',
    'index_p7_modules' => '⽤模块来拼接你⾃⼰的ClicBot吧',
    'index_p7_discover' => '',
    'index_p7_click' => '点击这里',

    'index_p8_introduction' => 'Introducing Demo Motion',
    'index_p8_the' => 'The easiest way to make your coolest friend',
    'index_p8_with' => 'With Demo Motion, programming a robot is easier than ever! Just move ClicBot in the motions you want it to perform, and it can remember and complete the entire process smoothly. You only need to consider what you want to do, and ClicBot will do the rest!',
    
    'index_p9_drag' => 'Drag & Drop graphical programming',
    'index_p9_look' => 'Looking for something a little more advanced? Our user-friendly drag & drop programming tool allows you to connect preconfigured ClicBot actions together.',

    'index_p10_clicbot' => 'ClicBot Community',
    'index_p10_share' => '跃跃欲试？不如下载一试！',
    'index_p10_community' => '在ClicBot APP中，社区是⼀个所有喜欢ClicBot的⼈可以分享有趣构型的地⽅！想做⼀些神奇的东⻄向全世界分享？仅需上传你的作品到ClicBot社区。如果你想找⼀些灵感，社区同样是个神奇的中⼼。看看别⼈创造的构型，把它们下载下来，然后⾃⼰试试吧！',

     
     'index_p11_create' => '创造独⼀⽆⼆的机器⼈？⾮常简单！',
     'index_p11_with_demo' => '在示教模式下，编程变得异常简单！仅仅让ClicBot做⼏个你想要它执⾏的动作，它就能把这些动作都记住并且流畅地执⾏整个过程。你只需要考虑你想做什么，剩下的统统交给ClicBot去完成！',

     'index_p11_go' => '从0到1学编程',
     'index_p11_look' => '想学⼀点更进阶的东⻄？我们拖拽式的图形化编程⼯具让你可以把预设好的动作连接在⼀起。就算⼀点都不会编程，你依然可以轻松地编程你的ClicBot！在拖拽式编程的过程中，你可以更加熟悉编程。现在编程ClicBot，没准将来编程太空⻜船。谁知道呢！',

     'index_p11_share' => '跃跃欲试？不如下载一试！',
     'index_p11_community' =>'在ClicBot APP中，社区是⼀个所有喜欢ClicBot的⼈可以分享有趣构型的地⽅！想做⼀些神奇的东⻄向全世界分享？仅需上传你的作品到ClicBot社区。如果你想找⼀些灵感，社区同样是个神奇的中⼼。看看别⼈创造的构型，把它们下载下来，然后⾃⼰试试吧！',

     'index_p12_please' => '订阅邮件以便在第一时间了解产品动态及促销活动！',
     'index_p12_your' => '你的邮箱',
     'index_p12_sub' => '订阅',

    // about us 

    'about_title' => 'Corporation Introduction',

    'about_content1' => 'Founded in 2014, KEYi Tech is an innovative robotics company with expertise in designing and developing STEAM Educational Robots (Science, Technology, Engineering, Arts, and Mathematics). Our team consists of top engineers and designers with previous experience from Samsung and Intel, with academic backgrounds from top universities worldwide. We own more than 40 independent intellectual properties ranging from robotic designs to AI algorithms.',

     'about_content2' => 'Our R&D department spent nearly 2 years working on ClicBot before showcasing at the 2020 CES conference in Las Vegas, reported by top media outlets like Forbes, Mashable, and TechCrunch. ClicBot has 50+ predefined use cases, 200+ reactions, and over 1000+ setups. Using our industrial level algorithms, ClicBot has smoother and more natural movements than other robots, giving it personality and making it a real companion.',
   
   'about_team' => 'Lead Team Introduction',

   'index_foot_email1' => '订阅邮件',
   'index_foot_email2' => '以便在第一时间了解产品动态及促销活动！',
   'index_foot_email3' => '',
   'index_foot_email' => 'info@keyirobot.com',
];