<?php
namespace app\index\controller;

use think\Controller;
use think\Request;

class Index extends Controller
{

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->redirect('https://www.keyirobot.com');
    }

    public function index()
    {

        // if (!is_mobile()) {
            return $this->fetch();
        // } else {
            // return $this->fetch('mobile');
        // }
    }
 
    public function indextest()
    {
        return $this->fetch();
    }

    public function repair()
    {
        return $this->fetch();
    }

    public function buy()
    {
        return $this->fetch();
    }
    public function testcontent()
    {
        
        return $this->fetch();
    }

    public function support()
    {
        return $this->fetch();
    }

    public function about()
    {

        return $this->fetch();
    }

    public function contact()
    {
        return $this->fetch();
    }

    public function foot()
    {
        return $this->fetch();
    }

    public function warranty()
    {
        return $this->fetch();
    }

    public function privacy()
    {
        return $this->fetch();
    }
    public function refund()
    {
        return $this->fetch();
    }
    public function shipping()
    {
        return $this->fetch();
    }

    public function hello($name = 'ThinkPHP5')
    {

        return 'sss,' . $name;

    }

    public function ok()
    {

        $str = 'upload/64/com_img/ssafds.0';

        $res_mes = pathinfo($str);
        $houzhui = $res_mes['extension'];
        if ($houzhui == '0') {
            $com_video_path = $res_mes['dirname'] . '/' . $res_mes['filename'] . '.mp4';
        }

    }

    public function oth()
    {
        return $this->fetch();
    }

    public function xilaideng(){
        return $this->fetch();
    }

}
