<?php

namespace app\index\controller;

use think\Controller;
use think\Db;
use think\Request;
use \think\facade\Lang;

class Web extends Controller
{

    protected $request;

    public $lan_bra;


    public function __construct(Request $request)
    {
        // // 种植cookie
        // if (!cookie('unique_flag')) {
        //     setcookie('unique_flag', ip2long(get_client_ip()) . time());
        // }

        header("Content-Type: text/html; charset=UTF-8");
        parent::__construct();
        $from = '';
        $refer = $_SERVER;
        if (isset($refer) && isset($refer['QUERY_STRING']) && isset($refer['QUERY_STRING'])) {
            $res = pathinfo($refer['QUERY_STRING']);
            $res = $this->convertUrlQuery($res['basename']);
            $from = isset($res['from']) ? $res['from'] : '';
        }

        $country = '美国';
        $country = $this->ipaddr();
        if ($country != '美国') {
            if ($from) {
                $country = 'https://shop.keyirobot.com/collections/all-products/products/clicbot-starter-kit-1?from=www.keyirobot.com&refer_orgin=' . $from;
            } else {
                $country = 'https://shop.keyirobot.com/collections/all-products/products/clicbot-starter-kit-1?from=www.keyirobot.com';
            }
        } else {
            if ($from) {
                $country = 'https://www.amazon.com/stores/page/AB86056E-37C7-4711-A875-26DACBE455DE?channel=LP-K&refer_orgin=' . $from;
            } else {
                $country = 'https://www.amazon.com/stores/page/AB86056E-37C7-4711-A875-26DACBE455DE?channel=LP-K';
            }
        }
        $country = addslashes($country);
        $this->assign('country', $country);

        $this->request = $request;

        $lang = cookie("think_var");
        if (!$lang) {
            $lang = $this->getRequestBrowserLang();
        }
        if (!$lang) {

            $lang = 'en';
        }
        $lang = 'en';

        switch ($lang) {

            case $lang == 'en';
                Lang::load('../application/index/lang/en-us.php');
                break;

            case $lang == 'ja':
                Lang::load('../application/index/lang/ja-jp.php');
                break;
            case $lang == 'zh':
                Lang::load('../application/index/lang/zh-ch.php');
                break;

            default:
                Lang::load('../application/index/lang/en-us.php');
        }

        $this->lan_bra = $lang;
        $this->assign('device', isios());
        cookie('think_var', $lang);
        $this->assign('lang', $lang);

    }

    public function convertUrlQuery($query)
    {
        $queryParts = explode('&', $query);
        $params = array();
        if ($queryParts) {
            foreach ($queryParts as $key => $value) {
                if (strchr($value, '=') !== false) {
                    $value = explode('=', $value);
                    $params[$value[0]] = $value[1];
                }
            }
        }
        return $params;
    }

    public function ipaddr()
    {
        // return 'ok';
        $ip = get_client_ip();
        // var_dump($ip);return;
        //        $request = Request::instance();
        //        $ip = $request->ip();
        // $ip = '99.56.136.197';
        $host = "https://hcapi20.market.alicloudapi.com";
        $path = "/ip";
        $method = "GET";
        $appcode = "fe70f7bb62594ba2aafcc29581848c74";
        $headers = array();
        array_push($headers, "Authorization:APPCODE " . $appcode);
        $querys = "ip=" . $ip;
        $bodys = "";
        $url = $host . $path . "?" . $querys;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_FAILONERROR, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt($curl, CURLOPT_HEADER, true);
        if (1 == strpos("$" . $host, "https://")) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        }
        $res = curl_exec($curl);
        $res = json_decode($res, true);

        if ($res) {
            if ($res['ret'] == 200) {
                $data['region'] = $res['data']['country_id'];
            } else {

            }
        }
        if ($res) {
            $country = $res['data']['country'];
        } else {
            $country = '';
        }

        return $country;
    }

    public function index()
    {
        return $this->fetch('kyads');

        // if (!is_mobile()) {
        //     return $this->fetch();
        // } else if (isios() == 'ios') {
        //     return $this->fetch('mobile');
        // } else {
        //     return $this->fetch('mobile_android');
        // }

    }

    public function company()
    {
        return $this->fetch('company');


    }
    public function newcompany()
    {
        return $this->fetch('newcompany');
    }
    public function newindex()
    {
        if (!is_mobile()) {
            return $this->fetch('newindex');
        } else {
            return $this->fetch('newandroid');
        }
    }

    public function main()
    {
        $country = $this->ipaddr();
        if ($country != '美国') {
            $country = 'https://shop.keyirobot.com/collections/all-products/products/clicbot-starter-kit-1?from=www.keyirobot.com';
        } else {
            $country = 'https://amzn.to/2KOiJ2R';
            $county = 'https://www.amazon.com/stores/page/AB86056E-37C7-4711-A875-26DACBE455DE?channel=LP-K';
        }

        if ($this->lan_bra == 'ja') {

            $country = 'https://clicbot-jp.myshopify.com/';
        }
        $this->assign('country', $country);

        if (!is_mobile()) {
            return $this->fetch();
        } else if (isios() == 'ios') {
            return $this->fetch('mobile');
        } else {
            return $this->fetch('mobile_android');
        }
    }

    public function indextest()
    {
        return $this->fetch();
    }

    public function _empty()
    {
        echo "empty";
    }

    public function testcontent()
    {
        //手写的测试的方法
        //http://clibot.test/index.php/index/web/testcontent.html
        $data = Db::table('test')->select();
        $data['time'] = time();
        $this->assign('data', $data);
        return $this->fetch();
    }

    public function del()
    {
        var_dump($_POST);
        exit;
        //return $this->fetch();
    }

    public function wocaoni()
    {
        $country = $this->ipaddr();
        if ($country != '美国') {
            $country = 'https://shop.keyirobot.com/collections/all-products/products/clicbot-starter-kit-1?from=www.keyirobot.com';
        } else {
            $county = 'https://www.amazon.com/stores/page/AB86056E-37C7-4711-A875-26DACBE455DE?channel=LP-K';
        }
        $this->assign('country', $country);
        return $this->fetch('kyads');
    }

    public function repair()
    {
        return $this->fetch();
    }

    public function buy()
    {
        return $this->fetch();
    }

    public function support()
    {

        return $this->fetch();
    }

    public function about()
    {

        return $this->fetch();
    }

    public function referral()
    {

        return $this->fetch();
    }

    public function conditions()
    {

        return $this->fetch();

    }

    public function contact()
    {
        return $this->fetch();
    }

    public function foot()
    {
        return $this->fetch();
    }

    public function warranty()
    {
        return $this->fetch();
    }

    public function privacy()
    {
        return $this->fetch();
    }

    public function refund()
    {
        return $this->fetch();
    }

    public function shipping()
    {
        return $this->fetch();
    }

    public function kyads()
    {
        return $this->fetch();
    }

    public function ajax_set()
    {
        return;
        $arr = $_POST['data'];
        $unique_flag = cookie('unique_flag'); // 用户的唯一标识
        if ($arr['glo_ajax_count'] == 1) {
            unset($arr['glo_ajax_count']);
            $arr['unique_flag'] = $unique_flag;
            $arr['refer'] = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
            $arr['create_time'] = time();
            $arr['create_ip'] = get_client_ip();
            Db::table('web_log')->insert($arr);
        } else {
            Db::table('web_log')->order('web_id desc')->where(['unique_flag' => $unique_flag])->limit(1)->update(['glo_ajax_time' => $this->msectime()]);
        }
    }

    public function getbrowser()
    {
        if (empty($_SERVER['HTTP_USER_AGENT'])) {
            return 'robot！';
        }
        if ((false == strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE')) && (strpos($_SERVER['HTTP_USER_AGENT'], 'Trident') !== false)) {
            return 'Internet Explorer 11.0';
        }
        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 10.0')) {
            return 'Internet Explorer 10.0';
        }
        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 9.0')) {
            return 'Internet Explorer 9.0';
        }
        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 8.0')) {
            return 'Internet Explorer 8.0';
        }
        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7.0')) {
            return 'Internet Explorer 7.0';
        }
        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6.0')) {
            return 'Internet Explorer 6.0';
        }
        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'Edge')) {
            return 'Edge';
        }
        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'Firefox')) {
            return 'Firefox';
        }
        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'Chrome')) {
            return 'Chrome';
        }
        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'Safari')) {
            return 'Safari';
        }
        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'Opera')) {
            return 'Opera';
        }
        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], '360SE')) {
            return '360SE';
        }
        //微信浏览器
        if (false !== strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessage')) {
            return 'MicroMessage';
        } else {
            return 'unknow';
        }
    }

    public function getRequestBrowserLang()
    {
        //只取前4位，这样只判断最优先的语言。
        //如果取前5位，可能出现en,zh的情况，影响判断。
        $lang = substr(isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : '', 0, 4);
        if (preg_match("/zh-c/i", $lang)) {
            return "zh";
        } else if (preg_match("/zh/i", $lang)) {
            return "zh-fan";
        } else if (preg_match("/en/i", $lang)) {
            return "en";
        } else if (preg_match("/fr/i", $lang)) {
            return "French";
        } else if (preg_match("/de/i", $lang)) {
            return "German";
        } else if (preg_match("/ko/i", $lang)) {
            return "Korean";
        } else if (preg_match("/es/i", $lang)) {
            return "Spanish";
        } else if (preg_match("/sv/i", $lang)) {
            return "Swedish";
        } else if (preg_match("/ja/i", $lang)) {
            return "ja";
        } else if (preg_match("/jp/i", $lang)) {
            return "ja";
        } else {
            //   echo $_SERVER["HTTP_ACCEPT_LANGUAGE"];
            return 'en';
        }
    }

}
