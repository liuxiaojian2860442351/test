<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

function get_lan(){
    return 'en';
    $lang = substr(isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])?$_SERVER['HTTP_ACCEPT_LANGUAGE']:'', 0, 4); //只取前4位，这样只判断最优先的语言。如果取前5位，可能出现en,zh的情况，影响判断。 
    if (preg_match("/zh-c/i", $lang) || preg_match("/zh,/i", $lang)){
        return 'zh';
    }else{
        return 'en';
    }

// if (preg_match("/zh-c/i", $lang)) 
// echo "简体中文"; 
// else if (preg_match("/zh/i", $lang)) 
// echo "繁體中文"; 
// else if (preg_match("/en/i", $lang)) 
// echo "English"; 
// else if (preg_match("/fr/i", $lang)) 
// echo "French"; 
// else if (preg_match("/de/i", $lang)) 
// echo "German"; 
// else if (preg_match("/jp/i", $lang)) 
// echo "Japanese"; 
// else if (preg_match("/ko/i", $lang)) 
// echo "Korean"; 
// else if (preg_match("/es/i", $lang)) 
// echo "Spanish"; 
// else if (preg_match("/sv/i", $lang)) 
// echo "Swedish"; 
// else echo $_SERVER["HTTP_ACCEPT_LANGUAGE"]; 

}

// 获取 IP时间戳
function get_client_ip($type = 0)
{
    $ip = '';
    if (getenv("HTTP_CLIENT_IP")) {
        $ip = getenv("HTTP_CLIENT_IP");
    } else if (getenv("HTTP_X_FORWARDED_FOR")) {
        $ip = getenv("HTTP_X_FORWARDED_FOR");
    } else if (getenv("REMOTE_ADDR")) {
        $ip = getenv("REMOTE_ADDR");
    } else {
        $ip = "未获取到";
    }

    return $ip;
}

 function isios(){
    if(strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone')||strpos($_SERVER['HTTP_USER_AGENT'], 'iPad')){
      return 'ios';
    }else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Android')){
      return 'android';
    }
}

function is_mobile()
{

    // returns true if one of the specified mobile browsers is detected
    // 如果监测到是指定的浏览器之一则返回true

    $regex_match = "/(nokia|iphone|android|motorola|^mot\-|softbank|foma|docomo|kddi|up\.browser|up\.link|";

    $regex_match .= "htc|dopod|blazer|netfront|helio|hosin|huawei|novarra|CoolPad|webos|techfaith|palmsource|";

    $regex_match .= "blackberry|alcatel|amoi|ktouch|nexian|samsung|^sam\-|s[cg]h|^lge|ericsson|philips|sagem|wellcom|bunjalloo|maui|";

    $regex_match .= "symbian|smartphone|midp|wap|phone|windows ce|iemobile|^spice|^bird|^zte\-|longcos|pantech|gionee|^sie\-|portalmmm|";

    $regex_match .= "jig\s browser|hiptop|^ucweb|^benq|haier|^lct|opera\s*mobi|opera\*mini|320x320|240x320|176x220";

    $regex_match .= ")/i";

    // preg_match()方法功能为匹配字符，既第二个参数所含字符是否包含第一个参数所含字符，包含则返回1既true
    return preg_match($regex_match, strtolower($_SERVER['HTTP_USER_AGENT']));
}

$is_mobile = is_mobile();

// $is_mobile = false;
// 应用公共文件
// 线上
// if ('www.clicbot.com' == $_SERVER['HTTP_HOST'] || 'clicbot.keyirobot.com' == $_SERVER['HTTP_HOST'] || 'www.keyirobot.com' == $_SERVER['HTTP_HOST'] || 'shop.keyirobot.com' == $_SERVER['HTTP_HOST'] || 'keyirobot.com' == $_SERVER['HTTP_HOST'] || 'clickbot.coolphper.com' == $_SERVER['HTTP_HOST'] || 'clicbot.coolphper.com' == $_SERVER['HTTP_HOST']) {
if(get_client_ip() != '61.233.30.930' || get_client_ip() == '121.69.45.182' || get_client_ip() == '127.0.0.1'  || 'www.clicbot.com' == $_SERVER['HTTP_HOST'] ){
/*
        if ($is_mobile) {

            define('WEB_INDEX_JS', 'https://file.keyirobot.com/static/index/mobile/js/');
            define('WEB_INDEX_CSS', 'https://file.keyirobot.com/static/index/mobile/css/');
            define('WEB_INDEX_IMG', 'https://file.keyirobot.com/static/index/mobile/img/');
            define('WEB_INDEX_VIDEO', 'https://file.keyirobot.com/static/index/mobile/video/');

        } else {
            define('WEB_INDEX_JS', 'https://file.keyirobot.com/static/index/js/');
            define('WEB_INDEX_CSS', 'https://file.keyirobot.com/static/index/css/');

            // define('WEB_INDEX_IMG', 'https://kybeijing-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/img/');


            define('WEB_INDEX_IMG', 'https://file.keyirobot.com/static/index/img/');
            define('WEB_INDEX_VIDEO', 'https://file.keyirobot.com/static/index/video/');
        }
*/

        if ($is_mobile) {

            define('WEB_INDEX_JS', '/static/index/js/');
            define('WEB_INDEX_CSS', '/static/index/css/');
            define('WEB_INDEX_IMG', '/static/index/mobile/img/');
            define('WEB_INDEX_VIDEO', '/static/index/mobile/video/');

            //company的图片的路径
            define('WEB_COMPANY_IMG', '/static/company/mobile/img/');
            //视频的路径
            define('WEB_COMPANY_VIDEO', '/static/company/mobile/video/');
        
        } else {
            define('WEB_INDEX_JS', '/static/index/js/');
            define('WEB_INDEX_CSS', '/static/index/css/');

            // define('WEB_INDEX_IMG', 'https://kybeijing-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/img/');


            define('WEB_INDEX_IMG', '/static/index/img/');
            //company的图片的路径
            define('WEB_COMPANY_IMG', '/static/company/img/');
            //视频的路径
            define('WEB_COMPANY_VIDEO', '/static/company/video/');

            define('WEB_INDEX_VIDEO', '/static/index/video/');
        }

}else{

    if (get_lan() == 'zh') {

        // if ($is_mobile) {
        //     define('WEB_INDEX_JS', 'https://kybeijing-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/mobile/js/');
        //     define('WEB_INDEX_CSS', 'https://kybeijing-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/mobile/css/');
        //     define('WEB_INDEX_IMG', 'https://kybeijing-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/mobile/img/');
        //     define('WEB_INDEX_VIDEO', 'https://kybeijing-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/mobile/video/');
        // } else {
        //     define('WEB_INDEX_JS', 'https://kybeijing-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/js/');
        //     define('WEB_INDEX_CSS', 'https://kybeijing-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/css/');
        //     define('WEB_INDEX_IMG', 'https://kybeijing-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/img/');
        //     define('WEB_INDEX_VIDEO', 'https://kybeijing-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/video/');
        // }


        if ($is_mobile) {
            define('WEB_INDEX_JS', 'https://kykj-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/mobile/js/');
            define('WEB_INDEX_CSS', 'https://kykj-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/mobile/css/');
            define('WEB_INDEX_IMG', 'https://kykj-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/mobile/img/');
            define('WEB_INDEX_VIDEO', 'https://kykj-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/mobile/video/');
        } else {
            define('WEB_INDEX_JS', 'https://kykj-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/js/');
            define('WEB_INDEX_CSS', 'https://kykj-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/css/');
            define('WEB_INDEX_IMG', 'https://kykj-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/img/');
            define('WEB_INDEX_VIDEO', 'https://kykj-1251538927.cos.ap-beijing.myqcloud.com/clicbot/static/index/video/');
        }

    } else {


        if ($is_mobile) {
            define('WEB_INDEX_JS', 'https://kymeiguo-1251538927.cos.na-siliconvalley.myqcloud.com/clicbot/static/index/mobile/js/');
            define('WEB_INDEX_CSS', 'https://kymeiguo-1251538927.cos.na-siliconvalley.myqcloud.com/clicbot/static/index/mobile/css/');
            define('WEB_INDEX_IMG', 'https://kymeiguo-1251538927.cos.na-siliconvalley.myqcloud.com/clicbot/static/index/mobile/img/');
            define('WEB_INDEX_VIDEO', 'https://kymeiguo-1251538927.cos.na-siliconvalley.myqcloud.com/clicbot/static/index/mobile/video/');
        } else {
            define('WEB_INDEX_JS', 'https://kymeiguo-1251538927.cos.na-siliconvalley.myqcloud.com/clicbot/static/index/js/');
            define('WEB_INDEX_CSS', 'https://kymeiguo-1251538927.cos.na-siliconvalley.myqcloud.com/clicbot/static/index/css/');
            define('WEB_INDEX_IMG', 'https://kymeiguo-1251538927.cos.na-siliconvalley.myqcloud.com/clicbot/static/index/img/');
            define('WEB_INDEX_VIDEO', 'https://kymeiguo-1251538927.cos.na-siliconvalley.myqcloud.com/clicbot/static/index/video/');
        }
    }



 


}



// } else {

//     // 局域网
//     define('WEB_INDEX_JS', '/clickbot/public/static/index/js/');
//     define('WEB_INDEX_CSS', '/clickbot/public/static/index/css/');
//     define('WEB_INDEX_IMG', '/clickbot/public/static/index/img/');
//     define('WEB_INDEX_VIDEO', '/clickbot/public/static/index/video/');

// }
