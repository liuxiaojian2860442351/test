// true 是 PC， false是手机
var ispc = ispc();
var ok_flag = true;
var t = 0,
    b = 0;
var animate_index = 1;
var scroll_dis = 0;
var org_b = 0;
var gun_b = 0;
var height = $(window).height();
var width = $(window).width();
var glo_height8 = height / 8; // glo_height8
var glo_height5 = height * 1.5; // glo_height5
var glo_height = height; // glo_height
var txt_all_width = $(".index_txt_all").width();
var txt_all_heigth = $(".index_txt_all").height();
var glo_sec = 0;
var glo_fumo = 0;
var glo_fumo_slow = 0;
var glo_txt = 0;
var glo_txt_ani = 0;
var glo_txt_ani_slow = 0;
// 控制导航 
var glo_up_down = 0;
var glo_down_up = 0;
var v_height = $(".head_index_video").height();
var v_width = $(".head_index_video").width();

var glo_tuxing = $(".index_tuxinghua").offset().top;
var glo_scroll_flag = true;

$(window).scroll(function() {
    // $(window).scrollTop($(".index_edu_cool").offset().top) 
    if(glo_first_load == 0){
        return;
    }
    // return;
    t = $(this).scrollTop();
    if (b < t) {
        if (t <= 0) {
            setTimeout(function() {
                b = t
            }, 0)
            // 阻止回弹效果,会影响手机端头部上下滑动初始值的判断，真是天才般的设计 酷酷酷!!!
            return;
        }
        // console.log("上滑：：：：", t);
        // 顶部导航展示,只针对手机和ipad
        if (!ispc) {
            glo_up_down = t;
            var tmp_top = t - glo_down_up >= 0 && t - glo_down_up <= 50 ? 50 - (t - glo_down_up) : 0;
            $(".header-nav-first").css({
                "position": "fixed",
                "top": tmp_top
            });
            $(".index_pic_video").css({
                "top": 50 + tmp_top
            })
        }
        // 只要上滑会隐藏搜索页面
        if ($(".head_search_page").css("display") == "block") {
            setTimeout(function() {
                if (!glo_search_input) {
                    return;
                }
                $(".head_search_page").css({
                    "position": "fixed",
                    "top": 0
                })
                $(".head_search_page").slideUp(500);
                $(".head_search_page").find("ul").slideUp(300);
            }, 10)
        }

        // 第一个模块，第一次滑动 1/8 触发
        if (b > glo_height8 && b <= glo_height5) {
            glo_down_fir++;
            // 第一次 所有都显示
            // 最外层的
            if (glo_down_fir == 1) {
                // clicbot + make happy 的控制
                $(".head_index_video").animate({
                    marginTop: (-v_height / 2 - 50) + "px"
                }, 1000, function(){
                    setTimeout(function(){
                        jsmpegplayer.play(); // 必须加到这里，放到回调里有时会白屏
                        $(".index_pic_bak").fadeOut(500);
                    }, 1000)
                    setTimeout(function(){
                        $("#canvas").fadeIn(300);
                    }, 1000)
                })
                // 视频弹窗 
                $(".head_video_title").animate({
                    opacity: "1"
                }, 600);
                $(".index_pic_bak").addClass("index_pic_bak_animate");
                setTimeout(function() {
                    glo_down_fir = 0;
                }, 1000)
            }
        }
        if (b > glo_height5 && b <= 2 * glo_height5) {
            ++glo_down_fir2;
            glo_scroll_flag = false;
            if (glo_down_fir2 == 1) {
                // 最外层的
                // clicbot + make happy 的控制
                $(".head_index_video").animate({
                    marginTop: (-v_height / 2 - 60) + "px"
                }, 1000, function() {
                    $(".head_index_video_title_clic").animate({ opacity: "0" }, 1000, function() {

                        $(".head_index_video_title_clic").css({
                            "visibility": "hidden"
                        });

                    })
                })
                // 视频弹窗 
                $(".head_video_title").animate({
                    opacity: "0"
                }, 300);
                setTimeout(function() {
                    // easy 文案
                    $(".head_video_last_txt").animate({
                        opacity: "1"
                    }, 600);
                }, 500)
                setTimeout(function() {
                    glo_down_fir2 = 0;
                }, 1000)

 

            }
        }
        if (b > 2 * glo_height5 && b <= 3 * glo_height5) {
            ++glo_down_sec;
            if (glo_down_sec == 1) {
                // $(".index_sec_fumo").fadeIn(800);
                $(".head_index_video").animate({
                    marginTop: (-v_height / 2 - 80) + "px",
                }, 1000);
                setTimeout(function() {
                    glo_down_sec = 0;
                }, 1000)

            }

        }
        if (b > 3 * glo_height5 && b <= 4 * glo_height5) {
            ++glo_down_sec7;
            if (glo_down_sec7 == 1) {
                $(".head_video_last_txt").animate({
                    opacity: 0,
                }, 300);
                setTimeout(function() {
                    glo_down_sec7 = 0;
                }, 1000) 

            }
        }

        if (b > 4 * glo_height5 && b <= 20 * glo_height5) {
            // $(".header-zhezhao").show();
            $(".index_sec_fumo").show();
            $(".index_pic_video").hide();
            jsmpegplayer.pause(); 
            ++glo_down_sec2;
            var glo_down_secx = 0;
            glo_fumo_slow = parseInt(glo_down_sec2);
            ++glo_down_secx;
            if (glo_down_secx == 1) {

                $(".head_index_video").animate({
                    marginTop: (-v_height / 2 - 250) + "px",
                    opacity: 0,
                }, 2000);

                setTimeout(function() {
                    glo_down_secx = 0;
                }, 50000)

            }
            if (glo_fumo_slow <= 239 && glo_down_sec2 == 1) {
                var tmp_time = 50;
                setTimeout(function() {
                    var index = $(".index_sec_fumo").find("img").attr("index");
                    if (index >= 239) {
                        if( b < 20*glo_height5){
                            $("html,body").scrollTop(20 * glo_height5);
                        }
                        glo_fumo_slow = 0;
                        return;
                        index = 238;
                    }
                    ++index;
                    tmp_time += 50;
                    if (index >= 40) {
                        // $(".index_sec_fumo").css({"align-items":"flex-end"})
                        // $(".index_sec_fumo img").css({"position":"relative", "top":"-50px"})
                    }
                    $(".index_sec_fumo").find("img").attr("index", index);
                    index = formatInt(index, 3);
                    $(".index_sec_fumo").find("img").attr("src", index_img + '/eays/a' + index + '.jpg');

                }, tmp_time)
                setTimeout(function() {
                    glo_down_sec2 = 0;
                }, 20)
            }
        }
        if (b>20 * glo_height5 && b <= 21 * glo_height5) {

            $(".head_index_video").hide();
            ++glo_down_sec3;
            if (glo_down_sec3 == 1) {

                $(".index_it_alive_txt").animate({
                    opacity: "1",
                    top: "50%"
                }, 2000);
                setTimeout(function() {
                    glo_down_sec3 = 0;
                }, 2000)
            }
        }
        if (b > 21 * glo_height5 && b <= 25 * glo_height5) {
            ++glo_down_sec4;
            if (glo_down_sec4 == 1) {
                $(".index_it_alive_txt").animate({
                    opacity: "0",
                    top: "-20px"
                }, 1000, function() {
                    $(".index_sec_fumo").fadeOut(1000);
                    setTimeout(function(){
                        $(".index_txt_all").show();
                        if(b < 25 * glo_height5){
                            $("html,body").scrollTop(25 * glo_height5);                            
                        }
                    }, 1000)
                });
                setTimeout(function() {
                    glo_down_sec4 = 0;
                }, 2000)
            }
        }

        if (b > 25 * glo_height5 && b <= 29 * glo_height5) {
            ++glo_down_sec10;
            $(".index_txt_all").css("opacity", 1);
            $(".index_txt_all").css("display", "flex");
            if (glo_down_sec10 == 1) {

                $(".index_txt_left").animate({
                    opacity: "1",
                    top: "-50px"
                }, 1000);
                $(".index_txt_right").animate({
                    opacity: "1",
                    top: "-30px"
                }, 2000, function(){
                        if(b < 29*glo_height5){
                            $("html,body").scrollTop(29 * glo_height5);
                        }
                });

                setTimeout(function() {
                    glo_down_sec10 = 0;
                }, 2000)
            }

        }

        if (b > 29 * glo_height5 && b <= 50 * glo_height5) {

            ++glo_down_sec5;
            $(".index_txt_pic").show();
            if (glo_down_sec5 == 1) {
                $(".index_txt_all").hide();
                $(".zong_div_bg").show();
                // $(".index_txt_video, #index_txt_video").show();
                // jsmpegindex_txt_video.play();

                $(".index_txt_left").animate({
                    opacity: "0",
                    top: "-60px"
                }, 500);
                $(".index_txt_right").animate({
                    opacity: "0",
                    top: "-80px"
                }, 500);
                $(".index_it_alive_bg").animate({
                    opacity: "0"
                }, 600)
                var glo_down_thr_slow = parseInt(glo_down_sec5 / 10);
                $(".index_txt_all").css("opacity", "0");
                var index = $(".index_txt_pic").find("img").attr("index");
                index = parseInt(index)
                if (index >= 158) {
                    glo_down_thr_slow = 0;
                    index = 157;
                    if(b< 50 * glo_height5){
                        $("html,body").scrollTop(50 * glo_height5)
                    }
                }
                ++index;
                if (index <= 45) {

                    $(".index_txt_all").css("opacity", "0");
                    $(".header-zhezhao").css({ "background-color": "white" })
                } else {
                    $(".header-zhezhao").css({ "background-color": "black" })
                    $(".header-zhezhao").hide();
                    // jsmpegindex_txt_video.animate({opacity:"1"}, 200);
                }
                $(".index_txt_pic").find("img").attr("index", index);
                if (index >= 60) {
                    $(".index_txt_pic").css({ "background-color": "none" });
                }
                index = formatInt(index, 5);
                $(".index_txt_pic").find("img").attr("src", index_img + '/ziti/1_' + index + '.png');
                setTimeout(function() {
                    glo_down_sec5 = 0;
                }, 40)
            }
        }

        console.log(parseInt(b / glo_height5), '----', parseInt(b / glo_height5) * 1.5 )

        if (b >= 50 * glo_height5 && b <= 58 * glo_height5) {

            ++glo_down_sec11;
            if (glo_down_sec11 == 1) {
                $(".index_txt_pic").fadeOut(900)
                $(".index_txt_pic img").fadeOut(800);
                // $(".index_txt_video").hide();
                $(".header-zhezhao").hide();
                $(".zong_div_bg").show();

                action_start();
                bgvideo();

                setTimeout(function() {

                    if( b < 58 * glo_height5){
                        $("html,body").scrollTop(58 * glo_height5)
                    }

                    glo_down_sec11 = 0;
                }, 7000)
            }

        }

        if (b > 58 * glo_height5 && b <= 60 * glo_height5) {
            $(".zong_div_bg").hide();
            $(".index_sec_fumo").hide();
            $(".index_xiaoche").show();
            ++glo_down_sec11;
            if (glo_down_sec11 == 1) {
                $(".index_many_top").animate({ top: "-100vh" }, 1500, function(){
                    $(".index_many_top").hide();
                    if(b < 60*glo_height5){
                        $("html,body").scrollTop(60*glo_height5);
                    }
                });
                setTimeout(function() {
                    glo_down_sec11 = 0;
                }, 2000)
            }
        }

        if (b >= 60 * glo_height5 && b <= 70 * glo_height5) {
                ++glo_down_sec12 + 10;
            $(".index_txt_pic, .zong_div_bg").hide();
            glo_fumo_slow = parseInt(glo_down_sec12);

            if (glo_fumo_slow <= 186 && glo_down_sec12 == 1) {
                $(".index_many_top").hide();
                var tmp_time = 50;
                setTimeout(function() {
                    var index = $(".index_xiaoche").find("img").attr("index");
                    if (index >= 186) {
                        index = 185;
                        glo_fumo_slow = 0;
                        if(b < 75*glo_height5){
                            // $(".index_xiaoche").hide();
                            // $("html,body").scrollTop(75 * glo_height5);
                            // $(window).scrollTop($(".index_module_div").offset().top - 100)
                            // $(window).scrollTop($(".index_tuxinghua").offset().top - 10)
                        }
                    }
                    ++index;
                    tmp_time += 50;
                    if (index >= 40) {
                        // $(".index_sec_fumo").css({"align-items":"flex-end"})
                        // $(".index_sec_fumo img").css({"position":"relative", "top":"-50px"})
                    }
                    $(".index_xiaoche").find("img").attr("index", index);
                    index = formatInt(index, 5);
                    $(".index_xiaoche").find("img").attr("src", index_img + '/part/b_' + index + '.jpg');

                }, tmp_time)
                setTimeout(function() {
                    glo_down_sec12 = 0;
                }, 20)
            }

        }
        if (b >= 70 * glo_height5 && b <= 76 * glo_height5) {
             
             ++glo_down_sec21;
   
             if(glo_down_sec21 == 1){
                $(".index_txt_all").css({"display":"none"})
                $(".index_many_top").hide();
                $(".index_xiaoche").hide();
                $(".index_module_div").css({ "display": "flex" })
                $("window").scrollTop($(".index_module_div").offset().top)
                setTimeout(function(){
                    glo_down_sec21 = 0;
                }, 1000)

                console.log($(".index_module_div").offset().top, ' ----' , b)
             
             }

           
        }

        if (b >90 * glo_height5 && b <= 92 * glo_height5 || b > glo_tuxing - 100) {
            $(".index_xiaoche").hide();
            $(".index_tuxinghua").show();
            $(".head_index_video, .index_txt_all").hide();
            ++glo_down_seven1;
            glo_down_thr1_slow = parseInt(glo_down_sec6 / 5);
            if (glo_down_seven1 == 1) {

                $(".phs_animate").css({ "display": "flex" })
                $(".phs_module_list").css("width", "0px");
                $(".phs_module_max_shu").css("width", "0px");
                var org_width = ["132px", "74px", "74px", "247px", "68px", "162px", "345px", "160px", "380px", "154px", "340px", "328px", "144px", "321px", "50px", "50px"];
                var phs_objs = $(".phs_module_list");
                phs_objs.push($(".phs_module_max_shu"));
                $(".phs_module_min_shu").css("padding", "0px");
                var mls = $(".phs_module_list");
                var time = 1000;
                var obj = '';
                var width = '';
                var obj_arr = [];
                for (var i = 0; i < phs_objs.length; ++i) {
                    if (i == phs_objs.length - 1 || i == phs_objs.length - 2) {
                        time += 20;
                    } else {
                        time += 10;
                    }
                    var obj = $(phs_objs[i]);
                    var width = org_width[i];

                    var j = i;
                    var mlsb = $(mls[i]);
                    obj_arr[i] = function(obj, time, width, mlsb, i) {
                        setTimeout(function() {
                            mlsb.animate({
                                padding: "0px 20px"
                            }, 50);
                            if (i == 4 || i == 7) {

                                obj.animate({
                                    width: (parseInt(width.replace("px", "")) + 20) + "px",
                                    height: "20px",
                                    opacity: 1
                                }, 800);

                                obj.animate({
                                    width: width,
                                    height: "20px",
                                    opacity: 1
                                }, 300);
                            } else {
                                obj.animate({
                                    width: (parseInt(width.replace("px", "")) + 20) + "px",
                                    height: "32px",
                                    opacity: 1
                                }, 800);

                                obj.animate({
                                    width: width,
                                    height: "30px",
                                    opacity: 1
                                }, 300);

                            }


                        }, time);
                    }
                    obj_arr[i](obj, time, width, mlsb, i);
                }

                time = 1500;
                var phs_img_obj_arr = [];
                var phs_img_objs = $(".phs_img");
                for (var i = 0; i < phs_img_objs.length; ++i) {
                    time += 180;
                    var obj = $(phs_img_objs[i]);
                    phs_img_obj_arr[i] = function(obj, time, i) {
                        setTimeout(function() {
                            obj.animate({
                                opacity: 1,
                            }, 2000);

                        }, time);
                    }
                    phs_img_obj_arr[i](obj, time, i);
                }

                time = 4500;
                var phs_last_arr = [];
                var phs_last_objs = $(".phs_last");
                for (var i = 0; i < phs_last_objs.length; ++i) {
                    time += 180;
                    var obj = $(phs_last_objs[i]);
                    phs_last_arr[i] = function(obj, time, i) {
                        setTimeout(function() {

                            obj = obj.addClass("phs_module_minwo_dh");

                        }, time);
                    }
                    phs_last_arr[i](obj, time, i);
                }

                // 左侧菜单栏动画：
                $(".phs_left").addClass("phs_left_width");
                setTimeout(function() {
                    $(".phs_left").find("li").css("width", "62px");
                    $(".phs_left").find("li").css("padding", "0px");
                    $(".phs_left").css("width", "62px");
                }, 10)
                $(".phs_left").find("li").css("visibility", "visible");
                tmp_time = 100;
                $(".phs_left").find("span").find("img").each(function() {
                    tmp_time += 50;
                    var tmp_obj = $(this);
                    setTimeout(function() {
                        tmp_obj.addClass("phs_left_img_scal")
                    }, tmp_time)
                })
                tmp_time = 300;
                $(".phs_left").find(".phs_left_txt").each(function() {
                    tmp_time += 50;
                    var tmp_obj = $(this);
                    setTimeout(function() {
                        tmp_obj.addClass("phs_left_scal")
                    }, tmp_time)
                });
            }


            setTimeout(function() {
                $(".phs_module_min_round, .phs_module_min_san, .phs_module_min").addClass("phs_module_min_round_animate")
                $(".phs_module_min_shu").animate({
                    padding: "65px 13px",
                    opacity: 1
                }, 800)

            }, 2000)
            // 处理模块的文案
            var phstxts = $(".phs_txt");
            var txt_arr = [];
            var phstxttime = 2550;
            for (var i = 0; i <= phstxts.length; ++i) {
                phstxttime += 100;
                var phstxt = phstxts[i];
                txt_arr[i] = function(phstxt, phstxttime) {
                    setTimeout(function() {
                        $(phstxt).slideDown(500);
                    }, phstxttime);
                }
                txt_arr[i](phstxt, phstxttime);
            }
            // 大竖线的显示
            setTimeout(function() {
                $(".phs_module_max_shu").animate({
                    padding: "202px 13px",
                    opacity: 1
                }, 600)
            }, 1100)

            // 小竖线的显示
            setTimeout(function() {
                $(".phs_module_min_shu").animate({
                    padding: "63px 15px",
                    opacity: 1
                }, 600)
            }, 1100)

            setTimeout(function() {
                glo_down_seven1 = 0;
            }, 100000)

        }

        if (b > 92 * glo_height5 && b <= 93 * glo_height5) {

            ++glo_down_sec13;
            if (glo_down_sec13 == 1) {

                $(".phs_right").fadeOut(1000);

                // 代码区域的效果 
                setTimeout(function() {
                    
                    $(".phs_code").show();
                    $(".phs_code_right").animate({
                        width: "100%"
                    }, 2000);
                    setTimeout(function() {
                        $(".phs_code").fadeIn(100);
                        var time = 0;
                        $(".phs_code_right").animate({
                            width: "100%"
                        }, 1200)
                        var objs = $(".phs_code_right span");
                        $(".phs_code_right li span").css("opacity", 1);
                        $(".phs_slidup").each(function(index) {
                            time = (index + 1) * 180;
                            var obj = $(this);
                            obj.animate({
                                top: "27px"
                            }, time);
                        })
                    }, 2000)

                }, 1200)

                setTimeout(function() {
                    glo_down_sec13 = 0;
                }, 4000);

            }

        }

        if (b > 93 * glo_height5 && b <= 94 * glo_height5) {

            ++glo_down_sec14;
            if (glo_down_sec14 == 1) {
                // $(".index_tuxinghua").animate({ top: "-120vh" }, 1000);
                setTimeout(function() {
                    glo_down_sec14 = 0;
                }, 3000)
            }

        }

        if (b > 93 * glo_height5 && b <= 94 * glo_height5) {

            ++glo_down_sec15;
            if (glo_down_sec15 == 1) {
                setTimeout(function() {
                    glo_down_sec15 = 0;
                }, 3000)
            }

        }

        if (b > 94 * glo_height5 && b <= 95 * glo_height5) {

            ++glo_down_sec16;
            if (glo_down_sec16 == 1) {
                // $(".index_module_div").animate({top:"-100vh"}, 2000, function(){
                //     // $(this).hide();
                // }); 
                setTimeout(function() {
                    glo_down_sec16 = 0;
                }, 2000)
            }

        }
        if (b >= 95 * glo_height5 && b <= 96 * glo_height5) {
            ++glo_down_sec17;
            if (glo_down_sec17 == 1) {
                $(".index_edu_cool").show();
            }
        }

        $(".index_list").each(function() {
            var index = $(this).index(".index_list");
            if (index <= 6) {
                // $(this).hide();
            }
        })

    } else {
        // console.log("下滑：============", t);
        if (!ispc) {
            if (t <= 0) {
                setTimeout(function() {
                    b = t
                }, 0)
                $(".header-nav-first").css({ "position": "fixed", "top": "50px" });
                $(".index_pic_video").css({ "position": "fixed", "top": "100px" });
                // 阻止回弹效果,会影响手机端头部上下滑动初始值的判断，真是天才般的设计 酷酷酷!!!
                return;
            }
            // 顶部导航收起, 只针对手机和ipad
            glo_down_up = t;
            var tmp_top = glo_up_down - t <= 50 && glo_up_down - t >= 0 ? glo_up_down - t : 50;
            $(".header-nav-first").css({
                "position": "fixed",
                "top": tmp_top
            });
            $(".index_pic_video").css({
                "top": tmp_top + 50
            })
        } else {



        }
        if (b >= 3 * glo_height5 && b <= 10 * glo_height5) {
            ++glo_up_sec1;
            var glo_up_sec1_slow = parseInt(glo_up_sec1 / 5);
            glo_up_sec1_slow = 90 - glo_up_sec1_slow;
            if (glo_up_sec1_slow <= 90) {
                var index = $(".index_sec_fumo").find("img").attr("index");
                if (index <= 1) {
                    index = 90;
                    glo_up_sec1_slow = 90;
                }
                --index;
                $(".index_sec_fumo").find("img").attr("index", index);
                index = formatInt(index, 5);
                $(".index_sec_fumo").find("img").attr("src", index_img + '/part/b_' + index + '.jpg');
            }
            setTimeout(function() {
                glo_up_sec1 = 0;
            }, 30000)
        }
        if (b >= 3 * glo_height5 && b <= 10 * glo_height5 + glo_height / 2) {
            $(".index_sec_fumo").fadeIn(800);
            $(".index_sec_fumo").find("img").show();
        }
        if (b >= 2 * glo_height5 && b <= 3 * glo_height5) {
            ++glo_up_sec;
            if (glo_up_sec == 1) {
                $(".index_sec_fumo").fadeOut(800);
                $(".head_index_video").css("opacity", 1)
                console.log($(".head_index_video").css("opacity"))
                // $(".head_index_video").animate({ marginTop: (-v_height / 2 - 40) + "px", opacity:"1"}, 1000);
                setTimeout(function() {
                    glo_up_sec = 0;
                }, 1000)
            }
        }
        if (b >= glo_height5 && b <= 2 * glo_height5) {
            ++glo_down_fir;
            $(".head_index_video_title_clic").css({
                "visibility": "visable"
            });
            if (glo_down_fir == 1) {
                // 最外层的
                // clicbot + make happy 的控制
                // $(".head_index_video").animate({ marginTop: (-v_height / 2 - 80) + "px" }, { queue: "pos_que", duration: 1500 })
                // $(".head_index_video").stop("pos_que");
                $(".head_index_video").animate({
                    marginTop: (-v_height / 2 - 20) + "px"
                }, 1000)
                // 视频弹窗
                $(".head_video_title").animate({
                    opacity: "0"
                }, 600);
                setTimeout(function() {
                    glo_down_fir = 0;
                }, 1000)
            }
        }
        if (b > glo_height8 && b <= glo_height5) {
            glo_up_fir2++;
            // 第一次 所有都显示
            // 最外层的
            // $(".head_index_video").css({ "position": "fixed", "margint-top": -v_height / 2 - 100 })
            if (glo_up_fir2 == 1) {
                // clicbot + make happy 的控制
                // $(".head_index_video").animate({ marginTop: (-v_height / 2 - 50) + "px" }, 600)
                $(".head_index_video").animate({
                    marginTop: (-v_height / 2) + "px"
                }, 1000)
                // 视频弹窗 
                $(".head_video_title").animate({
                    opacity: "1"
                }, 600);
                // easy文案隐藏
                $(".head_video_last_txt").animate({
                    opacity: "0"
                }, 800);
                $(".head_index_video_title_clic").css({
                    "visibility": "visible"
                });
                setTimeout(function() {
                    glo_up_fir2 = 0;
                }, 3000)
            }
        }
    }
    setTimeout(function() {
        b = t
    }, 0)
    // var index_education_two = $(".index_education_two").offset().top;
    // // // 如果滚动位置 》 动画所在位置开始替换图片
    // if (b - index_education_two - animate_index * 1 > 0 != false && ok_flag) {
    //     ok_flag = true;
    //     animate_index = $("#product_animate").attr("index");
    //     animate_index = parseInt(animate_index)
    //     if (gun_b == 0) {
    //         gun_b = b;
    //     }
    //     $("#product_animate").css({ "position": "fixed", "top": 0 })
    //     $(".index_education_two").css({ "position": "absolute", "top": 0 })
    //     // 如果滚动条滚动的距离超过图片切过的个数那么就OK停止
    //     if (animate_index >= 563) {
    //         animate_index = 1;
    //         // ok_flag = false;
    //         if (org_b == 0) {
    //             org_b = b;
    //         }
    //         var res = -(b - org_b);
    //         $("#product_animate").css({ "position": "fixed", "top": res })
    //         if (res < 0) {
    //             $("#product_animate").css({ "position": "relative" })
    //         }
    //     } else {
    //         animate_index += 2;
    //         $("#product_animate").attr("index", animate_index);
    //         if (animate_index >= 10) {
    //             $(".index_product_animate_txt_fir").addClass("index_product_fir_animate")
    //         }
    //         if (animate_index >= 220) {
    //             $(".index_product_animate_txt_sec").addClass("index_product_sec_animate")
    //         }
    //         // console.log(animate_index)
    //         animate_index = formatInt(animate_index, 3);
    //         $("#product_animate").attr("src", "/static/index/img/product_animate/a" + animate_index + ".jpg");
    //         $("#product_animate").css({ "position": "fixed", "top": 0 })
    //         $(".index_product_animate_txt").show();
    //     }
    // }
})


function bgvideo() {


   jsmpegmany();
   jsmpegindex_txt_video.play();
        
    $(".index_many_pic").show();
    $(".index_many_pic video").css({ "position": "fixed", "top": "50px" })
    $(".new_zong").css({ "position": "fixed", "top": "0px" });
    $(".top, .bottom").animate({ "height": "42.5vh" }, 4500);
    $(".a").animate({ "width": "42vw" }, 4500);
    $(".c").animate({ "width": "40vw" }, 4500);
    setTimeout(function() {

        $(".top, .bottom").animate({ "height": "38.4vh" }, 4000, "easeInOutQuint");
        $(".a, .c").animate({ "width": "38vw" }, 4000, "easeInOutQuint", function(){
            $(".zong_div_bg").hide();
        });
    }, 4600)

}


function action_start() {

    // return;

    $(".fir li:eq(0)").css("width", "18.6vw");
    $(".fir li:eq(1)").css("width", "14.1vw");
    $(".fir li:eq(2)").css("width", "18.6vw");
    $(".fir li:eq(3)").css("width", "14.1vw");
    $(".fir li:eq(4)").css("width", "18.6vw");

    setTimeout(function() {
        $(".fir li:eq(0)").animate({ left: "-10vw", top: "-16vh" }, 1000);
        $(".fir li:eq(1)").animate({ left: "17.1vw", top: "-16vh" }, 1000);
        $(".fir li:eq(2)").animate({ left: "40.4vw", top: "-16vh" }, 1000);
        $(".fir li:eq(3)").animate({ right: "17.1vw", top: "-16vh" }, 1000);
        $(".fir li:eq(4)").animate({ right: "-10vw", top: "-16vh" }, 1000);
    }, 1000)

    setTimeout(function() {
        $(".fir li:eq(0)").animate({ left: "-8vw", top: "-10vh" }, 1000);
    }, 4040)

    setTimeout(function() {
        $(".fir li:eq(1)").animate({ left: "18.2vw", top: "-8vh" }, 400);
    }, 4040)

    setTimeout(function() {
        $(".fir li:eq(2)").animate({ left: "40.4vw", top: "-8vh" }, 200);
    }, 4040)

    setTimeout(function() {
        $(".fir li:eq(3)").animate({ right: "18.2vw", top: "-8vh" }, 400);
    }, 4300)

    setTimeout(function() {
        $(".fir li:eq(4)").animate({ right: "-8vw", top: "-10vh" }, 1000);
    }, 4040)

    setTimeout(function() {

        $(".fir li:eq(0)").animate({ height: "23.2vh", width: "26.2vw", top: "-8vh" }, 1000);
        $(".fir li:eq(1)").animate({ height: "23.2vh", width: "19.2vw" }, 1000);
        $(".fir li:eq(2)").animate({ height: "23.2vh", width: "26.2vw", left: "37.2vw" }, 1000);
        $(".fir li:eq(3)").animate({ height: "23.2vh", width: "19.2vw" }, 1000);
        $(".fir li:eq(4)").animate({ height: "23.2vh", width: "26.2vw", top: "-8vh" }, 1000);
        $(".fir li img").animate({ height: "23.2vh" }, 1000)

    }, 5200)


    // 2
    $(".sec li:eq(0)").css("width", "14.1vw");
    $(".sec li").css("top", "-20vh");
    $(".sec li:eq(1)").css("width", "18.6vw");
    $(".sec li:eq(2)").css("width", "14.1vw");
    $(".sec li:eq(3)").css("width", "18.6vw");
    $(".sec li:eq(4)").css("width", "14.1vw");


    setTimeout(function() {
        $(".sec li:eq(0)").animate({ left: "-8vw", top: "15.2vh" }, 1000);
    }, 4040)

    setTimeout(function() {
        $(".sec li:eq(1)").animate({ left: "15.2vw", top: "15.2vh" }, 400);
    }, 4040)

    setTimeout(function() {
        $(".sec li:eq(2)").animate({ left: "43.4vw", top: "15.2vh" }, 200);
    }, 4040)

    setTimeout(function() {
        $(".sec li:eq(3)").animate({ right: "15.2vw", top: "15.2vh" }, 400);
    }, 4300)

    setTimeout(function() {
        $(".sec li:eq(4)").animate({ right: "-8vw", top: "15.2vh" }, 500);
    }, 4040)

    setTimeout(function() {
        $(".sec li:eq(0)").animate({ left: "-10vw", top: "12vh" }, 2000);
        $(".sec li:eq(1)").animate({ left: "10vw", top: "12vh" }, 2000);
        $(".sec li:eq(2)").animate({ left: "43.4vw", top: "12vh" }, 2000);
        $(".sec li:eq(3)").animate({ right: "10vw", top: "12vh" }, 2000);
        $(".sec li:eq(4)").animate({ right: "-10vw", top: "12vh" }, 2000);
    }, 2000)


    setTimeout(function() {

        $(".sec li:eq(0)").animate({ height: "23.2vh", width: "23.2vw" }, 2000);
        $(".sec li:eq(1)").animate({ height: "23.2vh", width: "23.2vw" }, 2000);
        $(".sec li:eq(2)").animate({ height: "23.2vh", width: "23.4vw", left: "38.4vw" }, 2000);
        $(".sec li:eq(3)").animate({ height: "23.2vh", width: "23.2vw" }, 2000);
        $(".sec li:eq(4)").animate({ height: "23.2vh", width: "23.2vw" }, 2000);
        $(".sec li img").animate({ height: "23.2vh" }, 2000)

    }, 5200)

    //3
    $(".thr li:eq(0)").css("width", "18.6vw");
    $(".thr li:eq(1)").css("width", "14.1vw");
    $(".thr li:eq(2)").css("width", "18.6vw");
    $(".thr li:eq(3)").css("width", "14.1vw");
    $(".thr li:eq(4)").css("width", "18.6vw");

    $(".thr li").css("top", "40vh");


    setTimeout(function() {
        $(".thr li:eq(0)").animate({ left: "-8vw", top: "38.4vh" }, 1000);
    }, 4040)

    setTimeout(function() {
        $(".thr li:eq(1)").animate({ left: "18.2vw", top: "38.4vh" }, 400);
    }, 4040)

    setTimeout(function() {
        $(".thr li:eq(2)").animate({ left: "40vw", top: "38.4vh" }, 200);
    }, 4040)

    setTimeout(function() {
        $(".thr li:eq(3)").animate({ right: "18.2vw", top: "38.4vh" }, 400);
    }, 4300)

    setTimeout(function() {
        $(".thr li:eq(4)").animate({ right: "-8vw", top: "38.4vh" }, 1000);
    }, 4040)

    setTimeout(function() {
        $(".thr li:eq(0)").animate({ left: "-10vw", top: "38.4vh" }, 2000);
        $(".thr li:eq(1)").animate({ left: "15vw", top: "38.4vh" }, 2000);
        $(".thr li:eq(2)").animate({ left: "40vw", top: "38.4vh" }, 2000);
        $(".thr li:eq(3)").animate({ right: "15vw", top: "38.4vh", right: "18.2vw" }, 2000);
        $(".thr li:eq(4)").animate({ right: "-10vw", top: "38.4vh" }, 2000);
    }, 2000)


    setTimeout(function() {

        $(".thr li:eq(0)").animate({ height: "23.2vh", width: "23.2vw" }, 2000);
        $(".thr li:eq(1)").animate({ height: "23.2vh", width: "23.2vw", left: "15.2vw" }, 2000);
        $(".thr li:eq(2)").animate({ height: "23.2vh", width: "23.2vw" }, 2000);
        $(".thr li:eq(3)").animate({ height: "23.2vh", width: "23.2vw", right: "15.2vw" }, 2000);
        $(".thr li:eq(4)").animate({ height: "23.2vh", width: "23.2vw" }, 2000);
        $(".thr li img").animate({ height: "23.2vh" }, 2000)

    }, 5200)

    // 4
    $(".four li:eq(0)").css("width", "14.1vw");
    $(".four li:eq(1)").css("width", "18.6vw");
    $(".four li:eq(2)").css("width", "14.1vw");
    $(".four li:eq(3)").css("width", "18.6vw");
    $(".four li:eq(4)").css("width", "14.1vw");

    $(".four li").css("top", "100vh");

    setTimeout(function() {
        $(".four li:eq(0)").animate({ left: "-8vw", top: "61.6vh" }, 1000);
    }, 4040)

    setTimeout(function() {
        $(".four li:eq(1)").animate({ left: "15.2vw", top: "61.6vh" }, 400);
    }, 4040)

    setTimeout(function() {
        $(".four li:eq(2)").animate({ left: "43.4vw", top: "61.6vh" }, 200);
    }, 4040)

    setTimeout(function() {
        $(".four li:eq(3)").animate({ right: "15.2vw", top: "61.6vh" }, 400);
    }, 4300)

    setTimeout(function() {
        $(".four li:eq(4)").animate({ right: "-8vw", top: "61.6vh" }, 1000);
    }, 4040)

    setTimeout(function() {
        $(".four li:eq(0)").animate({ left: "-10vw", top: "78vh" }, 2000);
        $(".four li:eq(1)").animate({ left: "10vw", top: "78vh" }, 2000);
        $(".four li:eq(2)").animate({ left: "43.4vw", top: "78vh" }, 2000);
        $(".four li:eq(3)").animate({ right: "10vw", top: "78vh" }, 2000);
        $(".four li:eq(4)").animate({ right: "-10vw", top: "78vh" }, 2000);
    }, 2000)

    setTimeout(function() {

        $(".four li:eq(0)").animate({ height: "23.2vh", width: "23.2vw" }, 2000);
        $(".four li:eq(1)").animate({ height: "23.2vh", width: "23.2vw" }, 2000);
        $(".four li:eq(2)").animate({ height: "23.2vh", width: "23.2vw", left: "38.4vw" }, 2000);
        $(".four li:eq(3)").animate({ height: "23.2vh", width: "23.2vw" }, 2000);
        $(".four li:eq(4)").animate({ height: "23.2vh", width: "23.2vw" }, 2000);
        $(".four li img").animate({ height: "23.2vh" }, 2000)

    }, 5200)

    // 5
    $(".five li:eq(0)").css("width", "18.6vw");
    $(".five li:eq(1)").css("width", "14.1vw");
    $(".five li:eq(2)").css("width", "18.6vw");
    $(".five li:eq(3)").css("width", "14.1vw");
    $(".five li:eq(4)").css("width", "18.6vw");
    $(".five li").css("top", "120vh");

    setTimeout(function() {
        $(".five li:eq(0)").animate({ left: "-8vw", top: "84.8vh" }, 1000);
    }, 4040)

    setTimeout(function() {
        $(".five li:eq(1)").animate({ left: "18.2vw", top: "84.8vh" }, 400);
    }, 4040)

    setTimeout(function() {
        $(".five li:eq(2)").animate({ left: "40.4vw", top: "84.8vh" }, 200);
    }, 4040)

    setTimeout(function() {
        $(".five li:eq(3)").animate({ right: "18.2vw", top: "84.8vh" }, 400);
    }, 4300)

    setTimeout(function() {
        $(".five li:eq(4)").animate({ right: "-8vw", top: "84.8vh" }, 1000);
    }, 4040)

    setTimeout(function() {
        $(".five li:eq(0)").animate({ left: "-10vw", top: "98vh" }, 2000);
        $(".five li:eq(1)").animate({ left: "10vw", top: "98vh" }, 2000);
        $(".five li:eq(2)").animate({ left: "40.4vw", top: "98vh" }, 2000);
        $(".five li:eq(3)").animate({ right: "10vw", top: "98vh" }, 2000);
        $(".five li:eq(4)").animate({ right: "-10vw", top: "98vh" }, 2000);
    }, 2000)

    setTimeout(function() {

        $(".five li:eq(0)").animate({ height: "23.2vh", width: "23.2vw" }, 2000);
        $(".five li:eq(1)").animate({ height: "23.2vh", width: "23.2vw", left: "15.2vw" }, 2000);
        $(".five li:eq(2)").animate({ height: "23.2vh", width: "23.2vw", left: "38.4vw" }, 2000);
        $(".five li:eq(3)").animate({ height: "23.2vh", width: "23.2vw", right: "15.2vw" }, 2000);
        $(".five li:eq(4)").animate({ height: "23.2vh", width: "23.2vw" }, 2000);
        $(".five li img").animate({ height: "23.2vh" }, 2000);
        $(".gif_bg, .zong").animate({ opacity: "1" }, 5000);

    }, 5200)



    setTimeout(function() {
        $(".index_many_top").show();
        $(".zong_div, .new_zong").animate({ opacity: "0" }, 1200, function(){
        });
        $(".index_many_pic").hide();
    }, 5200)


}

// 多模块介绍
$(".index_module_div img").mouseover(function() {

    $(this).removeClass("index_module_scale_min");
    $(this).addClass("index_module_scale_max");

})

$(".index_module_div img").mouseleave(function() {
    $(this).removeClass("index_module_scale_max");
    $(this).addClass("index_module_scale_min");
})

$(".index_module_div img").click(function() {
    if ($(".index_module_video").css("display") != "none") {

        $(".index_module_top").find("li").each(function() {
            var index = $(this).index(".index_module_top li");
            if (index == 1 || index == 2) {
                $(this).find("img").animate({ top: "0px" });
            } else if (index == 0) {
                $(this).find("img").animate({ top: "0px", left: "0px" });
            } else {
                $(this).find("img").animate({ top: "0px", right: "0px" });
            }
        })

        $(".index_module_bottom").find("li").each(function() {
            var index = $(this).index(".index_module_bottom li");
            if (index == 1 || index == 2) {
                $(this).find("img").animate({ top: "0px" });
            } else if (index == 0) {
                $(this).find("img").animate({ top: "0px", left: "0px" });
            } else {
                $(this).find("img").animate({ top: "0px", right: "-0px" });
            }
        })


        // $(".index_module_top").animate({ top: "0px" });
        // $(".index_module_bottom").animate({ bottom: "0px" });
        $(".index_module_middle li:eq(2)").animate({ right: "0px" })
        $(".index_module_middle li:eq(0)").animate({ left: "0px" });
        $(".index_module_video").animate({ width: "5px" }, 200)
        $(".index_module_video").hide();
    } else {

        // $(".index_module_top").animate({ top: "-20%" });
        // $(".index_module_bottom").animate({ bottom: "-20%" });


        $(".index_module_top").find("li").each(function() {
            var index = $(this).index(".index_module_top li");
            if (index == 1 || index == 2) {
                $(this).find("img").animate({ top: "-17vh" });
            } else if (index == 0) {
                $(this).find("img").animate({ top: "-17vh", left: "-8vw" });
            } else {
                $(this).find("img").animate({ top: "-17vh", right: "-8vw" });
            }
        })

        $(".index_module_bottom").find("li").each(function() {
            var index = $(this).index(".index_module_bottom li");
            if (index == 1 || index == 2) {
                $(this).find("img").animate({ top: "17vh" });
            } else if (index == 0) {
                $(this).find("img").animate({ top: "17vh", left: "-8vw" });
            } else {
                $(this).find("img").animate({ top: "17vh", right: "-8vw" });
            }
        })


        $(".index_module_middle li:eq(2)").animate({ right: "-8vw" })
        $(".index_module_middle li:eq(0)").animate({ left: "-8vw" });
        $(".index_module_video").attr("src", "static/index/video/video.mp4");
        $(".index_module_video").animate({ width: "55vw" }, 200)
        $(".index_module_video").show();

    }

})

$(".index_module_div").on("click", function(e) {

    if ($(e.target).closest(".index_module_video").length == 0 && $(e.target).closest("img").length == 0) {
        if ($(".index_module_video").css("display") == "none") {
            return;
        }
        if ($(".index_module_video").css("display") != "none") {
            // $(".index_module_top").animate({ top: "0px" });
            // $(".index_module_bottom").animate({ bottom: "0px" });
            // $(".index_module_middle li:eq(2)").animate({ right: "0px" })
            // $(".index_module_middle li:eq(0)").animate({ left: "0px" });


            $(".index_module_top").find("li").each(function() {
                var index = $(this).index(".index_module_top li");
                if (index == 1 || index == 2) {
                    $(this).find("img").animate({ top: "0px" });
                } else if (index == 0) {
                    $(this).find("img").animate({ top: "0px", left: "0px" });
                } else {
                    $(this).find("img").animate({ top: "0px", right: "0px" });
                }
            })

            $(".index_module_bottom").find("li").each(function() {
                var index = $(this).index(".index_module_bottom li");
                if (index == 1 || index == 2) {
                    $(this).find("img").animate({ top: "0px" });
                } else if (index == 0) {
                    $(this).find("img").animate({ top: "0px", left: "0px" });
                } else {
                    $(this).find("img").animate({ top: "0px", right: "-0px" });
                }
            })

            $(".index_module_middle li:eq(2)").animate({ right: "0px" })
            $(".index_module_middle li:eq(0)").animate({ left: "0px" });
            $(".index_module_video").animate({ width: "5px" }, 300)
            $(".index_module_video").hide();

        } else {

            $(".index_module_top").find("li").each(function() {
                var index = $(this).index(".index_module_top li");
                if (index == 1 || index == 2) {
                    $(this).find("img").animate({ top: "-17vh" });
                } else if (index == 0) {
                    $(this).find("img").animate({ top: "-17vh", left: "-8vw" });
                } else {
                    $(this).find("img").animate({ top: "-17vh", right: "-8vw" });
                }
            })

            $(".index_module_bottom").find("li").each(function() {
                var index = $(this).index(".index_module_bottom li");
                if (index == 1 || index == 2) {
                    $(this).find("img").animate({ top: "17vh" });
                } else if (index == 0) {
                    $(this).find("img").animate({ top: "17vh", left: "-8vw" });
                } else {
                    $(this).find("img").animate({ top: "17vh", right: "-8vw" });
                }
            })

            $(".index_module_middle li:eq(2)").animate({ right: "-8vw" })
            $(".index_module_middle li:eq(0)").animate({ left: "-8vw" });
            $(".index_module_video").attr("src", "static/index/video/video.mp4");
            $(".index_module_video").animate({ width: "55vw" }, 300)
            $(".index_module_video").show();
        }
    }

})