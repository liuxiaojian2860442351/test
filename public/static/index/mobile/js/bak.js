// true 是 PC， false是手机
var ispc = ispcfuc();
var ok_flag = true;
var t = 0,
    b = 0;
var animate_index = 1;
var scroll_dis = 0;
var org_b = 0;
var gun_b = 0;
var height = $(window).height();
var width = $(window).width();

var glo_height = height; // glo_height
var txt_all_width = $(".index_txt_all").width();
var txt_all_heigth = $(".index_txt_all").height();
var glo_sec = 0;
var glo_fumo = 0;
var glo_fumo_slow = 0;
var glo_txt = 0;
var glo_txt_ani = 0;
var glo_txt_ani_slow = 0;
// 控制导航 
var glo_up_down = 0;
var glo_down_up = 0;
var v_height = $(".head_index_video").height();
var v_width = $(".head_index_video").width();

var glo_tuxing = $(".index_tuxinghua").offset().top;
var glo_scroll_flag = true;
var glo_suoding_t;

$(function() {
    $(".index_list").hide();
    $(".head_index_video").show();
    $(".index_pic_video").show();
})

picsModule();

function picsModule() {

    $(".index_five1 li:eq(0)").css({ "top": "-60vh" });
    $(".index_five1 li:eq(1)").css({ "top": "-60vh" });
    $(".index_five1 li:eq(3)").css({ "bottom": "-60vh" });
    $(".index_five1 li:eq(4)").css({ "bottom": "-60vh" });

    $(".index_five2 li:eq(0)").css({ "top": "-60vh", "right": "-60vw" });
    $(".index_five2 li:eq(1)").css({ "top": "-60vh", "right": "-60vw" });
    $(".index_five2 li:eq(2)").css({ "top": "0vh", "right": "-60vw" });
    $(".index_five2 li:eq(3)").css({ "bottom": "-60vh", "right": "-60vw" });
    $(".index_five2 li:eq(4)").css({ "bottom": "-60vh", "right": "-60vw" });

}

function moduleInit() {
    $(".index_module_top li:eq(0) img").animate({ top: "-25vh", left: "-20vw" }); // 
    $(".index_module_top li:eq(1) img").animate({ top: "-30vh", right: "-8vw" }); //
    $(".index_module_top li:eq(2) img").animate({ top: "-22vh" }); //
    $(".index_module_top li:eq(3) img").animate({ top: "-25vh", right: "-18vw" }); //15 6

    $(".index_module_middle li:eq(0)").animate({ left: "-20vw" }); // -20
    $(".index_module_middle li:eq(2)").animate({ left: "20%" }); // 12%

    $(".index_module_bottom li:eq(0)").animate({ top: "25vh", left: "-20vw" }) // 6  10
    $(".index_module_bottom li:eq(1)").animate({ top: "25vh" }) // 15
    $(".index_module_bottom li:eq(2)").animate({ top: "22vh" }) // 15
    $(".index_module_bottom li:eq(3)").animate({ top: "26vh" }) // 16  -8
    $(".index_module_bottom li:eq(4)").animate({ top: "13vh", right: "-15vw" })
}

function moduleAnimate() {

    $(".index_module_top li img").stop(false, true);
    $(".index_module_middle li").stop(false, true);
    $(".index_module_bottom li").stop(false, true);

    $(".index_module_top li img").animate({ top: "0", left: "0px", right: "0px" }, 500);
    $(".index_module_middle li:eq(0), .index_module_middle li:eq(2)").animate({ top: "0", left: "0px", right: "0px" }, 500);
    $(".index_module_bottom li:eq(1), .index_module_bottom li:eq(4), .index_module_bottom li:eq(2), .index_module_bottom li:eq(3) ").animate({ top: "0px", right: "0px" }, 500);
    $(".index_module_bottom li:eq(0)").animate({ top: "0px", left: "0px" }, 500);
}


// 网站初始化
window.onload = function() {

    moduleInit();
    $(".head_index_video_title_clic, .head_index_video_title_happen").css({ "opacity": 1 })
    if (ispc) {
        $(".index_pic_bak").css({ "backgroundImage": 'url("/static/index/img/head_make_happy.png")' });
    } else {
        $(".index_pic_bak").css({ "backgroundImage": 'url("/static/index/mobile/img/head_make_happy.png")' });
    }
    setTimeout(function() {
        $(".head_index_video_title_clic, .head_index_video_title_happen").css({ "opacity": 1 })
        if (ispc) {
            $(".index_pic_bak").css({ "backgroundImage": 'url("/static/index/img/head_make_happy.png")' });
        } else {
            $(".index_pic_bak").css({ "backgroundImage": 'url("/static/index/mobile/img/head_make_happy.png")' });
        }
    }, 200)

    setTimeout(function() {
        bgvideo();
    }, 2000)

    setTimeout(function() {
        var res = $(window).scrollTop() / glo_height;

        if (res >= 30 * glo_height) {
            $('html,body').animate({ scrollTop: 34 * glo_height }, 500);
        } else {
            $('html,body').animate({ scrollTop: $(window).scrollTop() }, 500);
        }
    }, 10)
}

$(window).scroll(function() {
    //  生成圆元素位置：
    var res = $(".index_robots_round_li").offset().top - $(window).scrollTop();

    var index_community = $(".index_community").offset().top - $(window).scrollTop();

    if (glo_first_load == 0) {
        return;
    }

    t = $(this).scrollTop();

    glo_down_fir = parseInt(t / 40);
    org_glo_down_fir = parseInt(t);
    var glo_down_fir_opacity_no = '';
    glo_down_fir_val = glo_down_fir;

    var glo_vh = (t / height).toFixed(3);

    console.log("height:", glo_vh)

    if (b > t) {
        glo_down_sec1 = 0;
        glo_down_sec2 = 0;
        glo_down_sec3 = 0;
        glo_down_sec4 = 0;
        glo_down_sec5 = 0;
        glo_down_sec6 = 0;
        glo_down_sec7 = 0;
        glo_down_sec8 = 0;
        // glo_down_sec9 = 0;
        glo_down_sec10 = 0;
        glo_down_sec11 = 0;
        glo_down_sec12 = 0;
        glo_down_sec12 = 0;
        glo_down_sec13 = 0;
        setTimeout(function() {
            // glo_round_flag = 0;
        }, 3000)

        // setTimeout(function(){
        //     glo_down_sec7 = 0;
        // }, 800)
        // moduleInit();
    }

    // if (b != t) {

    if (t < 0) {
        setTimeout(function() {
            b = t
        }, 0)
        // 阻止回弹效果,会影响手机端头部上下滑动初始值的判断，真是天才般的设计 酷酷酷!!!
        return;
    }
    // 顶部导航展示,只针对手机和ipad
    if (!ispc) {
        glo_up_down = t;
        var tmp_top = t - glo_down_up >= 0 && t - glo_down_up <= 50 ? 50 - (t - glo_down_up) : 0;
        // tmp_top = tmp_top <= 5?0:tmp_top;
        $(".header-nav-first").css({
            "position": "fixed",
            "top": tmp_top
        });
        $(".index_pic_video").css({
            "top": 50 + tmp_top
        })
    }

    // 优化图片加载
    if (t > 20 * glo_height) {
        $(".index_module_div img").each(function() {
            if ($(this).attr("src") != $(this).attr("org_src")) {
                $(this).attr("src", $(this).attr("org_src"))
            }
        })
        $(".index_robots_pics img").each(function() {
            if ($(this).attr("src") != $(this).attr("org_src")) {
                $(this).attr("src", $(this).attr("org_src"))
            }
        })
    }
    if (t >= 8) {
        if ($(".index_txt_video_div").html() == '') {
            $(".index_txt_video_div").html('<video autoplay="autoplay" muted="" controls="true" id="index_pic_video" class="index_pic_video"><source org_src="/static/index/video/100-1.mp4" src="/static/index/video/100-1.mp4"></video>');
        }
    }

    if (t > 15 * glo_height) {
        // if($(".index_robots_video source").attr("src") != $(".index_robots_video source").attr("org_src")){
        // $(".index_robots_video source").attr("src", $(".index_robots_video source").attr("org_src"))
        // }
        if ($(".index_robots_one_video").html() == '') {
            $(".index_robots_one_video").html('<video autoplay="autoplay" muted="" controls="true" id="index_robots_video" class="index_robots_video"><source org_src="/static/index/video/100-1.mp4" src="/static/index/video/100-1.mp4"></video>');
        }

    }

      // 第一个模块的位置：
    // 缩放图片 和 显示 视频按钮  1h;
    if (t <= glo_height && t >= 0) {
        index_fir(t);
    }

    function index_fir(t) {
        // 第一个动画的文案
        $(".index_it_alive_txt").css({ "opacity": 0 });
        // 设置首屏的背景图片
        if (ispc) {
            $(".index_pic_bak").css({ "backgroundImage": 'url("/static/index/img/head_make_happy.png")' });
        } else {
            $(".index_pic_bak").css({ "backgroundImage": 'url("/static/index/mobile/img/head_make_happy.png")' });
        }
        // 设置首屏定格
        // $(".head_index_video").css({ "position": "fixed" })
        // 字体图片特殊处理，隐藏
        $(".index_list, .index_list_te").hide();
        // 设置首屏显示
        $(".head_index_video").show();
        // 所有的都显示
        $(".index_pic_bak, .head_index_video_title, .head_index_video").show();

        ++glo_down_sec1;
        // 解决刷新加载的时候的透明bug
        if (t <= glo_height / 2) {
            $(".head_index_video_title_happen, .head_index_video_title_clic").css({ "opacity": 1 })
        }
        if (glo_down_fir <= 5) {
            $(".head_video_title").css({ "opacity": 0 })
        }

        // 解决缩小动画
        if (glo_down_sec1 == 1) {

            $(".index_pic_bak").removeClass("index_pic_bak_animate_ni");
            $(".index_pic_bak").css("transform", "scale(1.1)");
            if(!$(".index_pic_bak").hasClass("index_pic_bak_animate")){
                $(".index_pic_bak").addClass("index_pic_bak_animate");                
            }

        }

        // 观看视频的文案的透明度的控制
        var res_fir = 0;
        // if (t < glo_height / 2) {
            var glo_down_fir_opacity = t / glo_height > 0.9 ? 1 : t / glo_height;
        // }
        glo_down_fir_opacity = glo_down_fir_opacity * 1;
        if (glo_down_fir_opacity <= 0.1) {
            // $(".head_video_title").hide();
        } else {
            $(".head_video_title").show();
        }
        res_fir = (t / glo_height) * 50;
        $(".head_video_title").attr("opacity")
        $(".head_index_video_title").css({ "marginTop": (-res_fir) + "px" });
        $(".head_video_title").css({ "opacity": glo_down_fir_opacity });
        $(".head_video_last_txt").css({ "opacity": "0" })
        $(".index_pic_bak").css({"opacity":"1"})
        setTimeout(function(){
            $(".head_index_video").css({"display":"block"});
        }, 200)

    }

    // 视频按钮 和 顶部 CLICBOT 消失, 并且 easy 文案显示 3h;
    if (t > glo_height && t <= 3 * glo_height) {
        $(".index_list, .index_list_te").hide();
        $(".head_index_video").show();
        $(".head_index_video_title").show();
        $(".head_index_video").css({ "position": "fixed" })
        if (ispc) {
            $(".index_pic_bak").css({ "backgroundImage": 'url("/static/index/img/head_make_happy.png")' });
        } else {
            $(".index_pic_bak").css({ "backgroundImage": 'url("/static/index/mobile/img/head_make_happy.png")' });
        }
        $(".head_video_last_txt").show();
        $(".index_fumo_pic").css("transform", "scale(9)");
        index_fir1(t);
    }

    function index_fir1(t) {

        $(".index_list").hide();
        $(".index_pic_bak").show();
        $(".head_index_video").show();
        $(".index_pic_video").show();
        $(".index_txt_all").hide();
        $(".index_pic_bak, .head_index_video_title, .head_index_video").show();

        // 隐藏视频
        if (t <= 2 * glo_height) {
            var glo_down_fir_opacity = (1 - t / (2 * glo_height)) < 0.1 ? 0 : (1 - t / (2 * glo_height));
            $(".head_video_title").css({ "opacity": glo_down_fir_opacity });
            $(".head_video_last_txt").css({"opacity":"0"})
        }
        // 显示 easy文案 ， 最后消失 clicbot
        if (t > 2 * glo_height && t < 2.6*glo_height) {
            var glo_down_fir_opacity_no = (t - 2 * glo_height) / glo_height;
            glo_down_fir_opacity_no = glo_down_fir_opacity_no * 5/3;
            res_top = -(t - 2 * glo_height) / (3 * glo_height) * 50 - 50;
            $(".head_video_title").css({ "opacity": 0 });
            $(".head_index_video_title").css({ "marginTop": (res_top) + "px" });
            $(".head_video_last_txt").css({ "opacity": glo_down_fir_opacity_no });
            $(".head_index_video_title_clic").css({ "opacity": 1 - glo_down_fir_opacity_no });
        }

        // 显示 easy文案 ， 最后消失 clicbot
        if (t > 2.6 * glo_height) {
            var glo_down_fir_opacity_no = 2.5*(3 * glo_height - t) / glo_height;
            $(".head_video_last_txt").css({ "opacity": glo_down_fir_opacity_no });
            $(".index_pic_bak").css({"opacity":glo_down_fir_opacity_no})
            $(".head_video_title").css({ "opacity": 0});
        }

    }

    // 抚摸图片显示，然后消失，视频显示
    if (t > 3 * glo_height && t <= 11 * glo_height) {
        // ++glo_down_sec5;
        // if (glo_down_sec5 == 1) {
        //     $(".index_list").hide();
        // }
        index_fir2(t);
        if ($(".index_sec_eay").attr("src") != '/static/index/img/sec/suo.png') {
            $(".index_sec_eay").css({ "position": "relative", "left": "0%", "top": "0%" });
        } else {
            $(".index_sec_eay").css({ "position": "relative", "left": "0%", "top": "10%" });
        }
    }

    function index_fir2(t) {
        $(".index_fumo_pic, .index_sec_fumo").show();
        $(".index_it_alive_txt").css({ "opacity": 0 });
        if (t > 3 * glo_height && t < 4 * glo_height) {
            $(".index_list_te").hide();
            $(".head_index_video").show();
            $(".head_video_last_txt, .head_index_video_title_clic").css({ "opacity": 0 });
            var glo_down_fir_opacity = (4 * glo_height - t) / glo_height;
            $(".head_index_video_title_happen").css({ "opacity": glo_down_fir_opacity });
            $(".head_index_video_title").css({ "marginTop": (-66 - 50 * ((t - 3 * glo_height) / glo_height)) })
            if (t > glo_height * 3.5) {
                // $(".index_pic_bak").fadeOut(500);
            }
            if (t > 3.9 * glo_height) {
                $(".head_index_video").show();
                $(".index_sec_fumo").show();
                $(".index_fumo_pic").show();
            }
        }

        if (t > 4 * glo_height && t < 6 * glo_height) {
            $(".index_list_te").hide();
            $(".index_pic_bak").hide();
            if (ispc) {
                $(".index_sec_eay").attr("src", '/static/index/img/sec/1.jpg');
            } else {
                $(".index_sec_eay").attr("src", '/static/index/mobile/img/sec/1.jpg');
            }
            $(".head_index_video_title").hide();
            $(".head_index_video").show();
            var sca = (6 * glo_height - t) / (2 * glo_height) * 8;
            $(".index_fumo_pic").css("transform", "scale(" + (1 + sca) + ")");
        }

        if (t > 6 * glo_height && t <= 10 * glo_height) {
            $(".index_list_te").hide();
            $(".index_pic_bak").hide();
            $(".index_it_alive_txt").hide();
            $(".head_index_video").show();
            $(".index_fumo_pic").css({ "transform": "scale(1)" });
            if (ispc) {

                var index = 82 * (1 - (10 * glo_height - t) / (4 * glo_height));
                index = parseInt(index);
                if (index >= 82) {
                    return;
                }
                if (index >= 0) {
                    ++index;
                    if (index == 1 && b < t) {
                        $(".index_fumo_pic").find("img").attr("src", '/static/index/img/sec/1.jpg');
                    } else {
                        $(".index_fumo_pic").find("img").attr("src", '/static/index/img/sec/' + index + '.png');
                    }
                }

            } else {

                var index = 100 * (1 - (10 * glo_height - t) / (4 * glo_height));
                index = parseInt(index);
                if (index >= 100) {
                    ++glo_down_sec2;
                    if (glo_down_sec2 == 1) {
                        setTimeout(function() {
                            $(".index_it_alive_txt").show();
                        }, 1000)
                    } else if (glo_down_sec2 >= 2) {
                        $(".index_it_alive_txt").show();
                    }
                    $(".index_fumovideo").css({ "position": "absolute", "top": "0px" })
                    $(".index_fumovideo").show();
                    $("#fumovideo").show();
                    return;
                }
                if (index >= 0) {
                    ++index;
                    if (index == 1 && b < t) {
                        $(".index_fumo_pic").find("img").attr("src", '/static/index/mobile/img/sec/1.jpg');
                    } else {
                        $(".index_fumo_pic").find("img").attr("src", '/static/index/mobile/img/sec/' + index + '.png');
                    }
                }
            }
        }

        if (t > 10 * glo_height && t <= 11 * glo_height) {
            $(".index_list_te").hide();
            ++glo_down_sec3;
            if (glo_down_sec3 == 1) {
                $(".index_sec_eay").css({ "position": "relative", "left": "-20%", "top": "20%" });
            }
            $(".index_it_alive_txt").show();
            $(".head_index_video").show();
            var res_left = (11 * glo_height - t) / glo_height;
            res_left = -20 * res_left + 20;
            var res_top = (11 * glo_height - t) / glo_height;
            res_top = 10 * (res_top / 2);
            $(".index_sec_eay").css({ "position": "relative", "left": res_left + "%", "top": (res_top) + "%" });
            // $(".index_fumo_pic").find("img").attr("src", '/static/index/img/sec/suo.png');
            var res_opacity = (11 * glo_height - t) / glo_height < 0.1 ? 0 : (11 * glo_height - t) / glo_height;
            $(".index_it_alive_txt").css({ "opacity": res_opacity, "marginTop": (100 - (1 - res_opacity) * 100) + "px" })
        }
    }

    if (t > 11 * glo_height && t <= 17 * glo_height) {
        ++glo_down_sec4;

        $(".index_list").hide();

        if (glo_down_sec4 == 1) {
            $(".index_sec_fumo").show();
            $(".index_fumo_pic").fadeOut(500);
            setTimeout(function() {
                $(".index_list").hide();
            }, 500)
        }
        $(".index_list_te").show();
        $(".index_txt_pic_bg").css({ "opacity": "1" })

        $(".index_txt_all").show();
        $(".index_txt_all").css({ "opacity": "1" })
        $(".index_txt_left, .index_txt_right").show();
        if (ispc) {

            if (t > 11 * glo_height && t <= 11.5 * glo_height) {
                var glo_down_fir_opacity = (1 - ((11.5 * glo_height - t) / (1 * glo_height)) <= 0.1) ? 0 : (1 - ((11.5 * glo_height - t) / (1 * glo_height)));
                $(".index_txt_left").css({ "marginTop": (50 - glo_down_fir_opacity * 50) + "%", "opacity": 1 });
                $(".index_txt_right").css({ "marginTop": (50 - glo_down_fir_opacity * 50) + "%" });
            }

            if (t > 11.5 * glo_height && t <= 12.5 * glo_height) {
                var glo_down_fir_opacity = (1 - ((12.5 * glo_height - t) / (1 * glo_height)) <= 0.1) ? 0 : (1 - ((12.5 * glo_height - t) / (1 * glo_height)));
                $(".index_txt_right").css({ "marginTop": (50 - glo_down_fir_opacity * 50) + "%", "opacity": glo_down_fir_opacity });
            }

        } else {

            // if (t > 11 * glo_height && t <= 11.5 * glo_height) {
            //     var glo_down_fir_opacity = (1 - ((11.5 * glo_height - t) / (1 * glo_height)) <= 0.1) ? 0 : (1 - ((11.5 * glo_height - t) / (1 * glo_height)));
            //     $(".index_txt_left").css({ "marginTop": 2*(50 - glo_down_fir_opacity * 50) + "%", "opacity": 1 });
            //     $(".index_txt_right").css({ "marginTop": (50 - glo_down_fir_opacity * 50) + "%" });
            // }

            if (t > 11 * glo_height && t <= 12.5 * glo_height) {
                var glo_down_fir_opacity = (1 - ((12.5 * glo_height - t) / (1.5 * glo_height)) <= 0.1) ? 0 : (1 - ((12.5 * glo_height - t) / (1.5 * glo_height)));
                $(".index_txt_left").css({ "marginTop": 2 * (50 - glo_down_fir_opacity * 50) + "%", "opacity": 1 });
                $(".index_txt_right").css({ "marginTop": (50 - glo_down_fir_opacity * 80) + "%", "opacity": glo_down_fir_opacity });
            }

        }


        if (t > 12.5 * glo_height && t <= 16 * glo_height) {
            $(".index_list").hide();
            $(".index_sec_fumo").hide();
            $(".index_txt_pic").show();
            $(".index_txt_all").hide();
            var index = 75 * (1 - ((16 * glo_height - t) / glo_height) / 3.5);
            index = parseInt(index);
            if (ispc) {
                if (index >= 75) {
                    index = 74;
                }

            } else {
                if (index >= 77) {
                    index = 76;
                }
            }

            ++index;
            if (index <= 45) {
                $(".index_txt_all").css("opacity", "0");
                // $(".index_txt_pic").css({ "background": "white" })
            } else {
                $(".index_many_pic, .index_many_video").show();
                $(".index_txt_video_div").show();
                // $(".index_txt_pic").css({ "background": "none" })
                $(".zong_div_bg").show();
            }

            // $(".index_txt_pic").find("img").attr("index", index);

            index = formatInt(index, 5);

            if (ispc) {
                $(".index_txt_pic").find("img").attr("src", index_img + '/ziti/1_' + index + '.png');
            } else {
                $(".index_txt_pic").find("img").attr("src", '/static/index/mobile/img/ziti/1_' + index + '.png');
            }

            if (index >= 75) {
                $(".index_many_pic").show();
                if (jsmpegindex_txt_video != undefined) {
                    jsmpegindex_txt_video.play();
                }
                // $(".index_txt_pic").css("background", "none");
            }

            if (index >= 45) {
                $(".index_txt_pic_bg").css({ "opacity": 0 })
                $(".index_many_pic img").hide();
                // 停止背景视频 和 隐藏图片
                if (jsmpegindex_txt_video != undefined) {
                    // jsmpegindex_txt_video.pause();
                    jsmpegindex_txt_video.play();
                }
                $(".zong_div_bg").show();
                $(".index_txt_video_div").show();
                $(".index_many_pic").show();
                $(".index_many_pic video").css({ "position": "fixed", "top": "50px" })
                $(".new_zong").css({ "position": "fixed", "top": "0px" });
                // $(".index_txt_video_div").css({ "width": res_fir_width + "vw" })
            }
        }

        if (t > 16 * glo_height && t <= 17 * glo_height) {
            picsModule();
            $(".index_list, .index_list_te").hide();
            $(".index_many_pic, .index_juzhen").show();
            var res_fir_width = 100 - 60 * (1 - (17 * glo_height - t) / glo_height);
            res_fir_width = res_fir_width <= 45 ? 40 : res_fir_width;
            $("#index_many_video").css({ "width": res_fir_width + "vw", "left": "5vw", "marginTop": "5vh", "height": "auto" })
        }

    }

    if (t > 16.5 * glo_height && t <= 18 * glo_height) {
        $(".index_list, .index_list_te").hide();
        $(".zong_div").show();
        $(".index_txt_video_div").show();
        $(".index_many_pic, .zong_div_bg").show();
        $(".zong_div_bg").show();
        $(".zong_div_bg").css({ "position": "fixed" })
        $(".index_juzhen").show();

        if (t < 17.5 * glo_height) {
            var res_top = -60 * ((17.5 * glo_height - t) / glo_height);
            $(".index_five1 li:eq(0)").css({ "top": res_top + "vh" });
            $(".index_five1 li:eq(1)").css({ "top": res_top + "vh" });
            $(".index_five1 li:eq(3)").css({ "bottom": res_top + "vh" });
            $(".index_five1 li:eq(4)").css({ "bottom": res_top + "vh" });

            $(".index_five2 li:eq(0)").css({ "top": res_top + "vh", "right": res_top + "vw" });
            $(".index_five2 li:eq(1)").css({ "top": res_top + "vh", "right": res_top + "vw" });
            $(".index_five2 li:eq(2)").css({ "top": "0vh", "right": res_top + "vw" });
            $(".index_five2 li:eq(3)").css({ "bottom": res_top + "vh", "right": res_top + "vw" });
            $(".index_five2 li:eq(4)").css({ "bottom": res_top + "vh", "right": res_top + "vw" });
        }

        if (t > 17.5 * glo_height) {
            var res_width = 10 * (18 * glo_height - t) / (0.5 * glo_height);
            $(".index_five img, .index_five li").css({ "width": (40 + res_width) + "vw" })
            var res_opacity = (18 * glo_height - t) / (0.5 * glo_height);
            $(".index_many_video").css({ "width": 40 + (res_width) + "vw" })
            $(".index_five img, .index_five li, .index_many_video, .index_many_pic").css({ "opacity": res_opacity });
            $(".index_pingjie_top").css({ "opacity": 1 - res_opacity })
            $(".index_pingjie_top").show();
        }
    }

    if (t > 17.5 * glo_height && t < 18.5 * glo_height) {

        $(".index_list").hide();
        if (t <= 18.5 * glo_height) {
            $(".index_pingjie_zong").show();
            $(".index_pingjie_txt").css({ "marginTop": -50 * ((t - 18.5 * glo_height) / glo_height) + "px" })
            // if (t > 18 * glo_height) {
            //     $(".index_pingjie_zong").css({ "top": -100 * (1 - (18.5 * height - t) / (0.5 * glo_height)) + "vh" })
            // }
        }

    }

    if (t > 20 * glo_height && t <= 21 * glo_height) {
        ++glo_down_sec3;
        var res_opacity = (t - 20 * glo_height) / glo_height;
        if (glo_down_sec3 == 1) {
            $(".index_pingjie_zong").show();
            // $(".index_pingjie_zong").css({ "opacity": res_opacity })
        } else if (glo_down_sec3 >= 2) {
            $(".index_pingjie_zong").show();
            // $(".index_pingjie_zong").css({ "opacity": 1 });
        }
        $(".index_pingjie_zong").show();
        $(".index_pingjie_zong").css({"opacity":1})
        var res_opacity = 1 - (t - 20*glo_height)/glo_height;
        var res_top = 100*(t - 20*glo_height)/glo_height;
        $(".index_pingjie_top").css({"top": -res_top + "vh"})
        $(".index_pingjie_txt").css("opacity", res_opacity);
        // $(".index_pingjie_top").css({ "opacity": res_opacity })
    }

    if (t >= 21 * glo_height && t <= 30 * glo_height) {
        $(".index_list").hide();
        if (t <= 22 * glo_height) {
            $(".index_pingjie_zong").show();
            $(".index_pingjie_txt").css({ "marginTop": -50 * ((t - 21 * glo_height) / glo_height) + "px" })
            if (t > 21.5 * glo_height) {
                $(".index_pingjie_zong").css({ "top": -100 * (1 - (22 * height - t) / (0.5 * glo_height)) + "vh" })
            }
            $(".index_xiaoche").show();
        }
        if (t > 22 * glo_height) {
            $(".index_xiaoche").show();

            $(".index_xiaoche").find("img").attr("index", index);
            // index = formatInt(index, 5);
            if (ispc) {

                index = 205 * (1 - (30 * glo_height - t) / (8 * glo_height));
                index = parseInt(index);
                ++index;
                $(".index_xiaoche").find("img").attr("src", index_img + '/part/' + index + '.png');
            } else {
                index = 118 * (1 - (30 * glo_height - t) / (8 * glo_height));
                index = parseInt(index);
                ++index;
                $(".index_xiaoche").find("img").attr("src", '/static/index/mobile/img/part/' + index + '.png');
            }
        }
    }

    if(t > 30*glo_height && t<40*glo_height){
        $(".index_list").hide();
        $(".index_xiaoche").show();
        var res_top = (33*glo_height - t) * 100 /glo_height;
        $(".index_xiaoche").css("position", "fixed");
        $(".index_robots_pics_middle, .index_robots_section").show();
        $(".index_robots_section").css({"top":res_top + "vh"});
    }
    
    if(t > 36*glo_height && t <= 37*glo_height){
        $(".index_list, .index_list_te").hide();
        $(".index_robots_section, .index_robots_dh").show();
        $(".index_robots_dh").css({"position":"fixed"})
        var res_top = -50*(1 - (37*glo_height - t)/glo_height);
        var res_opacity = (37 * glo_height - t)/glo_height;
        $(".index_robots_dh").css({"top": 100*(37*glo_height - t)/glo_height + "vh" } )
        $(".index_robots_idea_div, .index_robots_txt").css({"opacity":res_opacity, "marginTop":res_top + "px"});
    }

    if(t > 37*glo_height && t <= 38*glo_height){
        $(".index_list, .index_list_te").hide();
        $(".index_robots_section, .index_robots_dh").show();
        $(".index_robots_dh").css({"position":"fixed"})
        var res_top = -50*(1 - (37*glo_height - t)/glo_height);
        var res_opacity = 1 - (37 * glo_height - t)/glo_height;
        $(".index_robots_idea_div, .index_robots_txt").css({"opacity":res_opacity, "marginTop":res_top + "px"});
    }
    
    if(t > 38*glo_height &&  t <= 43 * glo_height) {
        $(".index_list").hide();
        $(".index_last_static").show();
        if (t < 39 * glo_height) {
            $(".index_robots_dh").show();
            var res_opacity = (t - 38 * glo_height) / glo_height > 0.9 ? 1 : (t - 38 * glo_height) / glo_height;
            $(".index_module_txt").css({ "opacity": res_opacity });
            $(".index_last_static").css({ "position": "fixed", "top": "0px" })

            if (t > 38.5 * glo_height) {
                ++glo_down_sec7;
                if (glo_down_sec7 == 1) {
                    moduleAnimate();
                    setTimeout(function() {
                        glo_down_sec7 = 0;
                    }, 600)
                }
            }

        }
        if (t >= 39 * glo_height) {
            ++glo_down_sec8;
            if (glo_down_sec8 == 1) {
                moduleInit();
                setTimeout(function() {
                    glo_down_sec8 = 0;
                }, 600)
            }
            $(".index_robots_dh").hide();
        }
        if (t >= 39 * glo_height && t <= 40 * glo_height) {
            $(".index_module_txt").css({ "opacity": 1 });
            var res_fir = (40 * glo_height - t) / glo_height;
            var opacity = (1 - res_fir) < 0.1 ? 0 : (1 - res_fir);
            $(".index_module_txt").css({ "opacity": opacity })
            $(".index_last_static").css({ "top": -100 * (1 - res_fir) + "vh" })
        }

        if (t >= 41 * glo_height && t <= 42 * glo_height) {
            $(".index_module_txt").css({ "opacity": 1 });
            var res_fir = (42 * glo_height - t) / glo_height;
            var opacity = (1 - res_fir) < 0.1 ? 0 : (1 - res_fir);
            $(".index_module_txt").css({ "opacity": opacity })
            $(".index_last_static").css({ "top": -100 - 100 * (1 - res_fir) + "vh" })
        }

        if (t >= 43 * glo_height && t <= 44 * glo_height) {
            $(".index_module_txt").css({ "opacity": 1 });
            var res_fir = (44 * glo_height - t) / glo_height;
            var opacity = (1 - res_fir) < 0.1 ? 0 : (1 - res_fir);
            $(".index_module_txt").css({ "opacity": opacity })
            $(".index_last_static").css({ "top": -200 - 100 * (1 - res_fir) + "vh" })
        }
        // if(t>=44*glo_height && t <= 44 * glo_height + 395){            
        //     var res_fir = t - 44*glo_height;
        //     $(".index_last_static").css({"top": -2*glo_height - res_fir});
        //     $(".all-footer").css({"bottom": (-395 + res_fir) + "px"})
        // }
    }
     
    if (t >= 44 * glo_height && t <= 44 * glo_height + 395) {
        var res_fir = t - 44 * glo_height;
        $(".index_last_static").css({ "top": -3 * glo_height - res_fir });
        // $(".all-footer").css({"bottom": (-395 + res_fir) + "px"})
    }

    setTimeout(function() {
        b = t
    }, 0)


    return;

    // 抚摸图片显示，然后消失，视频显示


    if (t >= 27 * glo_height && t <= 28 * glo_height) {
        $(".index_list").hide();
        $(".index_last_static").show();
        $(".index_module_div").show();
        var res_fir = (33 * glo_height - t) / glo_height;
        var opacity = (1 - res_fir) < 0.1 ? 0 : (1 - res_fir);
        $(".index_module_txt").css({ "opacity": opacity })
    }

    if (t >= 28 * glo_height && t <= 29 * glo_height) {
        $(".index_list").hide();
        $(".index_last_static").show();
        $(".index_module_div").show();
        var res_fir = 1 - (39 * glo_height - t) / glo_height;
        $(".index_last_static").css({ "top": -100 * res_fir + "vh" });
    }
    if (t >= 30 * glo_height && t <= 31 * glo_height) {
        $(".index_list").hide();
        $(".index_last_static").show();
        $(".index_module_div").show();
        var res_fir = 1 - (31 * glo_height - t) / glo_height;
        $(".index_last_static").css({ "top": -200 - (100 * res_fir) + "vh" });
        $(".index_tuxinghua").css({ "top": "0px", "position": "fixed" })
            ++glo_down_sec5;
        if (glo_down_sec5 == 1) {
            tuxinghua();
            setTimeout(function() {
                // glo_down_sec5 = 0;
            }, 6000)
            setTimeout(function() {
                $(".phs_animate_ul_li_content").slideUp(500);
                $(".phs_animate_easy").slideDown(500)
            }, 1000)
        }
    }

    if (t >= 31 * glo_height && t <= 32 * glo_height) {
        $(".index_list").hide();
        $(".index_last_static").show();
        $(".index_module_div").show();
        $(".index_tuxinghua").css({ "top": "0px", "position": "fixed" })
        var res_fir = 1 - (32 * glo_height - t) / glo_height;
        $(".index_last_static").css({ "top": -300 - (100 * res_fir) + "vh" });
        ++glo_down_sec6;
        if (glo_down_sec6 == 1) {
            $(".phs_right").fadeOut(1000);

            setTimeout(function() {

                $(".phs_code").show();
                $(".phs_code_right").animate({
                    width: "100%"
                }, 2000);
                setTimeout(function() {
                    $(".phs_code").fadeIn(100);
                    var time = 0;
                    $(".phs_code_right").animate({
                        width: "100%"
                    }, 1200)
                    var objs = $(".phs_code_right span");
                    $(".phs_code_right li span").css("opacity", 1);
                    $(".phs_slidup").each(function(index) {
                        time = (index + 1) * 180;
                        var obj = $(this);
                        obj.animate({
                            top: "27px"
                        }, time);
                    })
                }, 2000)

                $(".phs_animate_ul_li").slideUp(500);
                $(".phs_animate_easy").slideUp(500)
                $(".phs_compatible").show();
                $(".phs_compatible_title").slideDown(500);
                $(".phs_compatible_after").slideDown(500);

            }, 1000)

        }
    }
    if (t >= 32 * glo_height && t <= 33 * glo_height) {
        ++glo_down_sec7;
        if (glo_down_sec7 == 1) {
            $(".phs_compatible_after").slideUp(500);
            $(".phs_compatible_only").slideDown(500);
        }

        var res_fir = 1 - (33 * glo_height - t) / glo_height;

        $(".index_tuxinghua").css({ "top": -100 * res_fir + "vh", "position": "fixed" })
        $(".index_community").css({ "top": 100 - 100 * res_fir + "vh", "position": "fixed" })

        setTimeout(function() {
            glo_down_sec7 = 0;
        }, 6000)
    }
    if (t >= 33 * glo_height && t <= 33.5 * glo_height) {
        var res_fir = 1 - (33.5 * glo_height - t) / glo_height;
        $(".index_community").css({ "top": -50 * res_fir + "vh", "position": "fixed" })
    }
    if (t >= 37.5 * glo_height + 392) {
        $(".index_section1").html($(".index_last_static").html());
        $(".index_section1 div").css({ "position": "relative", "top": "0px" })
    } else {
        $(".index_section1").html("");
    }


    // console.log("glo_bal: glo_down_fir", glo_down_fir);

    if (glo_down_fir >= 1030400 && glo_down_fir <= 1204400) {

        $(".index_list").css("display", "none");
        $(".index_last_static").show();
        $(".index_module_div").show();
        var res = t - 850 * 20;
        if (res >= 1700) {
            $(".phs_animate").show();
        }
        $(".index_last_static").css({ "top": (50 - res) + "px" });

    }

    if (glo_down_fir >= 1000400 && glo_down_fir <= 1204000) {

        var res_fir = 850 - glo_down_fir;
        org_res_fir = res_fir / 2;
        res_fir = 50 - res_fir;
        glo_round_flag = 2;
        res_opacity = 1 - 0.04 * res_fir;
        if (glo_down_fir > 800 && glo_down_fir < 825) {
            $(".index_robots_idea_div").css({ "opacity": res_opacity, "top": (50 - (25 - org_res_fir)) + "vh" });
            $(".index_robots_txt").css({ "opacity": res_opacity, "top": (60 - (25 - org_res_fir) + "vh") })
        }

        if (glo_down_fir >= 900) {
            $(".index_tuxinghua").css({ "position": "fixed", "top": "0px" })
            $(".index_last_static").css({ "position": "fixed", "top": "100vh" })
                ++glo_down_sec23;
        }

        if (glo_down_fir >= 900 && glo_down_sec23 == 1 && glo_down_fir <= 920) {
            ++glo_down_sec23;
            tuxinghua();
            setTimeout(function() {
                // glo_down_sec23 = 0;
            }, 6000)
        }

        if (glo_down_fir >= 950 && glo_down_fir <= 1000) {
            $(".phs_animate_ul_li_content").slideUp(500);
            $(".phs_animate_easy").slideDown(500)
        }

        if (glo_down_fir >= 1000 && glo_down_fir <= 1050) {
            $(".phs_animate_ul_li").slideUp(500);
            $(".phs_animate_easy").slideUp(500)
            $(".phs_compatible").show();
            $(".phs_compatible_title").slideDown(500);
            $(".phs_compatible_after").slideDown(500);
            ++glo_down_sec24;
            if (glo_down_sec24 == 1) {
                $(".phs_right").fadeOut(1000);
                // 代码区域的效果 
                setTimeout(function() {
                    $(".phs_code").show();
                    $(".phs_code_right").animate({
                        width: "100%"
                    }, 2000);
                    setTimeout(function() {
                        $(".phs_code").fadeIn(100);
                        var time = 0;
                        $(".phs_code_right").animate({
                            width: "100%"
                        }, 1200)
                        var objs = $(".phs_code_right span");
                        $(".phs_code_right li span").css("opacity", 1);
                        $(".phs_slidup").each(function(index) {
                            time = (index + 1) * 180;
                            var obj = $(this);
                            obj.animate({
                                top: "27px"
                            }, time);
                        })
                    }, 2000)
                }, 1200)
            }

        }

        if (glo_down_fir >= 1045430 && glo_down_fir < 1104430) {
            $(".phs_compatible_after").slideUp(500);
            $(".phs_compatible_only").slideDown(500);
        }

        if (glo_down_fir >= 1104430 && glo_down_fir <= 142400) {
            var res_fir = 1200 - glo_down_fir;
            res_fir = 100 - res_fir;
            $(".index_tuxinghua").css({ "position": "relative", "top": "0px" })
            $(".index_last_static").css({ "position": "fixed", "top": (-200 - res_fir) + "vh" })
        }
        if (glo_down_fir >= 1200) {
            $(".index_tuxinghua").css({ "position": "relative", "top": "0px" })
            $(".index_last_static").css({ "position": "relative", "top": "-300vh" })
        }

    }

    //

    // 顶部导航展示,只针对手机和ipad
    if (!ispc) {
        glo_up_down = t;
        var tmp_top = t - glo_down_up >= 0 && t - glo_down_up <= 50 ? 50 - (t - glo_down_up) : 0;
        $(".header-nav-first").css({
            "position": "fixed",
            "top": tmp_top
        });
        $(".index_pic_video").css({
            "top": 50 + tmp_top
        })
    }

    // 只要上滑会隐藏搜索页面
    if ($(".head_search_page").css("display") == "block") {
        setTimeout(function() {
            if (!glo_search_input) {
                return;
            }
            $(".head_search_page").css({
                "position": "fixed",
                "top": 0
            })
            $(".head_search_page").slideUp(500);
            $(".head_search_page").find("ul").slideUp(300);
        }, 10)
    }


    // } else {




    // }








})


chushihua();

function chushihua() {
    $(".fir li:eq(0)").css("width", "18.6vw");
    $(".fir li:eq(1)").css("width", "14.1vw");
    $(".fir li:eq(2)").css("width", "18.6vw");
    $(".fir li:eq(3)").css("width", "14.1vw");
    $(".fir li:eq(4)").css("width", "18.6vw");
    $(".fir li").css("top", "-30vh");

    $(".sec li:eq(0)").css("width", "14.1vw");
    $(".sec li:eq(1)").css("width", "18.6vw");
    $(".sec li:eq(2)").css("width", "14.1vw");
    $(".sec li:eq(3)").css("width", "18.6vw");
    $(".sec li:eq(4)").css("width", "14.1vw");
    $(".sec li").css("top", "-50vh");

    $(".thr li:eq(0)").css({ "width": "18.6vw", "left": "-73vw" });
    $(".thr li:eq(1)").css({ "width": "14.1vw", "left": "-40.73vw" });
    $(".thr li:eq(2)").css({ "width": "18.6vw" });
    $(".thr li:eq(3)").css({ "width": "14.1vw", "right": "-40.73vw" });
    $(".thr li:eq(4)").css({ "width": "18.6vw", "right": "-73vw" });
    $(".thr li").css("top", "0vh");

    // 4
    $(".four li:eq(0)").css("width", "14.1vw");
    $(".four li:eq(1)").css("width", "18.6vw");
    $(".four li:eq(2)").css("width", "14.1vw");
    $(".four li:eq(3)").css("width", "18.6vw");
    $(".four li:eq(4)").css("width", "14.1vw");
    $(".four li").css("top", "50vh");

    // 5
    $(".five li:eq(0)").css("width", "18.6vw");
    $(".five li:eq(1)").css("width", "14.1vw");
    $(".five li:eq(2)").css("width", "18.6vw");
    $(".five li:eq(3)").css("width", "14.1vw");
    $(".five li:eq(4)").css("width", "18.6vw");
    $(".five li").css("top", "30vh");


}


function bgvideo() {

    jsmpegmany();
    if (jsmpegindex_txt_video != undefined) {
        jsmpegindex_txt_video.play();
    }

    $(".index_many_pic").show();
    $(".index_many_pic video").css({ "position": "fixed", "top": "50px" })
    $(".new_zong").css({ "position": "fixed", "top": "0px" });

}

function action_start() {

}

// 多模块介绍
$(".index_module_div img").mouseover(function() {

    $(this).removeClass("index_module_scale_min");
    $(this).addClass("index_module_scale_max");

})

$(".index_module_div img").mouseleave(function() {
    $(this).removeClass("index_module_scale_max");
    $(this).addClass("index_module_scale_min");
})

$(".index_module_div img").click(function() {
    if ($(".index_module_video").css("display") != "none") {

        $(".index_module_top").find("li").each(function() {
            var index = $(this).index(".index_module_top li");
            if (index == 1 || index == 2) {
                $(this).find("img").animate({ top: "0px" });
            } else if (index == 0) {
                $(this).find("img").animate({ top: "0px", left: "0px" });
            } else {
                $(this).find("img").animate({ top: "0px", right: "0px" });
            }
        })

        $(".index_module_bottom").find("li").each(function() {
            var index = $(this).index(".index_module_bottom li");
            if (index == 1 || index == 2) {
                $(this).find("img").animate({ top: "0px" });
            } else if (index == 0) {
                $(this).find("img").animate({ top: "0px", left: "0px" });
            } else {
                $(this).find("img").animate({ top: "0px", right: "-0px" });
            }
        })

        // $(".index_module_top").animate({ top: "0px" });
        // $(".index_module_bottom").animate({ bottom: "0px" });
        $(".index_module_middle li:eq(2)").animate({ right: "0px", left: "0px" })
        $(".index_module_middle li:eq(0)").animate({ left: "0px" });
        $(".index_module_video").animate({ width: "5px" }, 200)
        $(".index_module_video").hide();
    } else {

        // $(".index_module_top").animate({ top: "-20%" });
        // $(".index_module_bottom").animate({ bottom: "-20%" });

        $(".index_module_top").find("li").each(function() {
            var index = $(this).index(".index_module_top li");
            if (index == 2) {
                $(this).find("img").animate({ top: "-15vh" });
            } else if (index == 0) {
                $(this).find("img").animate({ top: "-15vh", left: "-8vw" });
            } else if (index == 3) {
                $(this).find("img").animate({ top: "-15vh", right: "-6vw" });
            } else if (index == 1) {
                $(this).find("img").animate({ top: "-22vh", right: "-8vw" });
            } else {
                $(this).find("img").animate({ top: "-15vh", right: "-8vw" });
            }
        })

        $(".index_module_bottom").find("li").each(function() {
            var index = $(this).index(".index_module_bottom li");
            if (index == 1 || index == 2) {
                $(this).find("img").animate({ top: "15vh" });
            } else if (index == 0) {
                $(this).find("img").animate({ top: "6vh", left: "-10vw" });
            } else if (index == 4) {
                $(this).find("img").animate({ top: "3vh", right: "-12vw" });
            } else if (index == 1) {
                $(this).find("img").animate({ top: "-22vh", right: "-8vw" });
            } else {
                $(this).find("img").animate({ top: "16vh", right: "-8vw" })
            }
        })

        $(".index_module_middle li:eq(2)").animate({ right: "-8vw", left: "12%" })
        $(".index_module_middle li:eq(0)").animate({ left: "-13vw" });
        $(".index_module_video").attr("src", "static/index/video/video.mp4");
        $(".index_module_video").animate({ width: "55vw" }, 200)
        $(".index_module_video").show();
    }

})

$(".index_module_div").on("click", function(e) {

    if ($(e.target).closest(".index_module_video").length == 0 && $(e.target).closest("img").length == 0) {
        if ($(".index_module_video").css("display") == "none") {
            return;
        }
        if ($(".index_module_video").css("display") != "none") {
            // $(".index_module_top").animate({ top: "0px" });
            // $(".index_module_bottom").animate({ bottom: "0px" });
            // $(".index_module_middle li:eq(2)").animate({ right: "0px" })
            // $(".index_module_middle li:eq(0)").animate({ left: "0px" });


            $(".index_module_top").find("li").each(function() {
                var index = $(this).index(".index_module_top li");
                if (index == 2) {
                    $(this).find("img").animate({ top: "0px" });
                } else if (index == 0) {
                    $(this).find("img").animate({ top: "0px", left: "0px" });
                } else if (index == 1) {
                    $(this).find("img").animate({ top: "0vh", right: "-8vw" });
                } else {
                    $(this).find("img").animate({ top: "0px", right: "0px" });
                }
            })

            $(".index_module_bottom").find("li").each(function() {
                var index = $(this).index(".index_module_bottom li");
                if (index == 1 || index == 2) {
                    $(this).find("img").animate({ top: "0px" });
                } else if (index == 0) {
                    $(this).find("img").animate({ top: "0px", left: "0px" });
                } else if (index == 1) {
                    $(this).find("img").animate({ top: "-22vh", right: "-8vw" });
                } else {
                    $(this).find("img").animate({ top: "0px", right: "-0px" });
                }
            })

            $(".index_module_middle li:eq(2)").animate({ right: "0px", left: "0px" })
            $(".index_module_middle li:eq(0)").animate({ left: "0px" });
            $(".index_module_video").animate({ width: "5px" }, 300)
            $(".index_module_video").hide();

        } else {

            $(".index_module_top").find("li").each(function() {
                var index = $(this).index(".index_module_top li");
                if (index == 2) {
                    $(this).find("img").animate({ top: "-17vh" });
                } else if (index == 0) {
                    $(this).find("img").animate({ top: "-17vh", left: "-8vw" });
                } else if (index == 1) {
                    $(this).find("img").animate({ top: "-22vh", right: "-8vw" });
                } else {
                    $(this).find("img").animate({ top: "-17vh", right: "-8vw" });
                }
            })

            $(".index_module_bottom").find("li").each(function() {
                var index = $(this).index(".index_module_bottom li");
                if (index == 1 || index == 2) {
                    $(this).find("img").animate({ top: "17vh" });
                } else if (index == 0) {
                    $(this).find("img").animate({ top: "17vh", left: "-8vw" });
                } else if (index == 3) {
                    $(this).find("img").animate({ top: "16vh", right: "-8vw" });
                } else {
                    $(this).find("img").animate({ top: "17vh", right: "-8vw" });
                }
            })

            $(".index_module_middle li:eq(2)").animate({ right: "-8vw", left: "0px" })
            $(".index_module_middle li:eq(0)").animate({ left: "-13vw" });
            $(".index_module_video").attr("src", "static/index/video/video.mp4");
            $(".index_module_video").animate({ width: "55vw" }, 300)
            $(".index_module_video").show();
        }
    }

})


function tuxinghua() {

    $(".phs_animate").css({ "display": "flex" })
    $(".phs_module_list").css("width", "0px");
    $(".phs_module_max_shu").css("width", "0px");
    var org_width = ["132px", "74px", "74px", "365px", "68px", "162px", "345px", "160px", "380px", "154px", "340px", "328px", "144px", "321px", "50px", "50px"];
    var phs_objs = $(".phs_module_list");
    phs_objs.push($(".phs_module_max_shu"));
    $(".phs_module_min_shu").css("padding", "0px");
    var mls = $(".phs_module_list");
    var time = 1000;
    var obj = '';
    var width = '';
    var obj_arr = [];
    for (var i = 0; i < phs_objs.length; ++i) {
        if (i == phs_objs.length - 1 || i == phs_objs.length - 2) {
            time += 20;
        } else {
            time += 10;
        }
        var obj = $(phs_objs[i]);
        var width = org_width[i];

        var j = i;
        var mlsb = $(mls[i]);
        obj_arr[i] = function(obj, time, width, mlsb, i) {
            setTimeout(function() {
                mlsb.animate({
                    padding: "0px 20px"
                }, 50);
                if (i == 4 || i == 7) {

                    obj.animate({
                        width: (parseInt(width.replace("px", "")) + 20) + "px",
                        height: "20px",
                        opacity: 1
                    }, 800);

                    obj.animate({
                        width: width,
                        height: "20px",
                        opacity: 1
                    }, 300);
                } else {
                    obj.animate({
                        width: (parseInt(width.replace("px", "")) + 20) + "px",
                        height: "32px",
                        opacity: 1
                    }, 800);

                    obj.animate({
                        width: width,
                        height: "30px",
                        opacity: 1
                    }, 300);

                }


            }, time);
        }
        obj_arr[i](obj, time, width, mlsb, i);
    }

    time = 1500;
    var phs_img_obj_arr = [];
    var phs_img_objs = $(".phs_img");
    for (var i = 0; i < phs_img_objs.length; ++i) {
        time += 180;
        var obj = $(phs_img_objs[i]);
        phs_img_obj_arr[i] = function(obj, time, i) {
            setTimeout(function() {
                obj.animate({
                    opacity: 1,
                }, 2000);

            }, time);
        }
        phs_img_obj_arr[i](obj, time, i);
    }

    time = 4500;
    var phs_last_arr = [];

    // var phs_last_objs = $(".phs_last");
    // for (var i = 0; i < phs_last_objs.length; ++i) {
    //     time += 180;
    //     var obj = $(phs_last_objs[i]);
    //     phs_last_arr[i] = function(obj, time, i) {
    //         setTimeout(function() {

    //             obj = obj.addClass("phs_module_minwo_dh");

    //         }, time);
    //     }
    //     phs_last_arr[i](obj, time, i);
    // }

    // 左侧菜单栏动画：
    $(".phs_left").addClass("phs_left_width");
    setTimeout(function() {
        $(".phs_left").find("li").css("width", "62px");
        $(".phs_left").find("li").css("padding", "0px");
        $(".phs_left").css("width", "62px");
    }, 10)
    $(".phs_left").find("li").css("visibility", "visible");
    tmp_time = 100;
    $(".phs_left").find("span").find("img").each(function() {
        tmp_time += 50;
        var tmp_obj = $(this);
        setTimeout(function() {
            tmp_obj.addClass("phs_left_img_scal")
        }, tmp_time)
    })
    tmp_time = 300;
    $(".phs_left").find(".phs_left_txt").each(function() {
        tmp_time += 50;
        var tmp_obj = $(this);
        setTimeout(function() {
            tmp_obj.addClass("phs_left_scal")
        }, tmp_time)
    });



    setTimeout(function() {
        $(".phs_module_min_round, .phs_module_min_san, .phs_module_min").addClass("phs_module_min_round_animate")
        $(".phs_module_min_shu").animate({
            padding: "65px 13px",
            opacity: 1
        }, 800)

    }, 2000)
    // 处理模块的文案
    var phstxts = $(".phs_txt");
    var txt_arr = [];
    var phstxttime = 2550;
    for (var i = 0; i <= phstxts.length; ++i) {
        phstxttime += 100;
        var phstxt = phstxts[i];
        txt_arr[i] = function(phstxt, phstxttime) {
            setTimeout(function() {
                $(phstxt).slideDown(500);
            }, phstxttime);
        }
        txt_arr[i](phstxt, phstxttime);
    }
    // 大竖线的显示
    setTimeout(function() {
        $(".phs_module_max_shu").animate({
            padding: "202px 13px",
            opacity: 1
        }, 600)
    }, 1100)

    // 小竖线的显示
    setTimeout(function() {
        $(".phs_module_min_shu").animate({
            padding: "63px 15px",
            opacity: 1
        }, 600)
    }, 1100)

    setTimeout(function() {
        glo_down_seven1 = 0;
    }, 100000)



}