<?php /*a:3:{s:67:"D:\phpstudy_pro\WWW\website\application\index\view\web\contact.html";i:1611298838;s:69:"D:\phpstudy_pro\WWW\website\application\index\view\.\public\head.html";i:1611566809;s:69:"D:\phpstudy_pro\WWW\website\application\index\view\.\public\foot.html";i:1614218332;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <title>
        ClicBot
    </title>
    <meta content="webkit" name="renderer">
    <meta charset="utf-8">
    <meta name="keywords" content="ClicBot, KEYi TECH, STEAM, STEM, Educational robot" />
    <meta name="description" content="ClicBot is a modular robot with personality designed for STEM education. ClicBot can be built into millions of configurations and programmed by coders of all experience levels." />
    <meta content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" name="viewport">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no">
    <link rel="shortcut icon" href="<?php echo htmlentities(WEB_INDEX_IMG); ?>logo.png" type="image/x-icon" />
    <link href="<?php echo htmlentities(WEB_INDEX_CSS); ?>global.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo htmlentities(WEB_INDEX_CSS); ?>index.css">
    <link rel="stylesheet" type="text/css" href="<?php echo htmlentities(WEB_INDEX_CSS); ?>contact.css">
    <style type="text/css">
        .all-footer{
            margin: 0px;
            height: 360px;
        }
    </style>
    
</head>

<body>
    <script src="<?php echo htmlentities(WEB_INDEX_JS); ?>tool/jquery.js" type="text/javascript"></script>
<style>
    @font-face {
    font-weight: 100;
    font-family: 'DINPro-Regular';
    src: url(../../../static/font/DINPro-Regular.otf)
}



@font-face {
    font-weight: 100;
    font-family: 'DINPro-Medium';
    src: url(../../../static/font/DINPro-Medium.otf)
}

@font-face {
    font-weight: 100;
    font-family: 'DINPro-Black';
    src: url(../../../static/font/DINPro-Black.otf)
}

@font-face {
    font-weight: 700;
    font-family: 'DINPro-Bold';
    src: url(../../../static/font/DINPro-Bold.otf)
}

@font-face {
    font-weight: 100;
    font-family: 'DINPro-Light';
    src: url(../../../static/font/DINPro-Light.otf) 
 }
 

 *{
    font-family: DINPro-Regular;
 }
 
#mc_embed_signup input.mce_inline_error{
    border-color: white !important;
}
 
.top_buy{
    display: inline-block;
           cursor: pointer;
    font-weight: 100 !important;
    border-radius: 15px;
    padding: 0px 15px;
    height: 22px;
    line-height: 20px;
    display: inline-block;
    font-family: 'DINPro-Medium';
    font-size: 14px;
    color: white;
    background-color: #1D66E7;
    margin: 0 auto; 
}
  
#index-sub-float-title{
    font-size: 14px;
    font-family: DINPro-Light !important;
    font-weight: 100;
    color: #B4B4B4;
    display: none;
}

#index-mce-responses{
    margin:15px auto;
    color: #A5A6AA;
}
.header-nav-first ul li a{
    font-size: 16px;
}
.header-nav-first ul{
    width: 1000px;
}
@media screen and (max-width: 1024px){
    .header-nav-first ul{
        width: 700px;
    }
}
/*.zhong-list a{
    color: white;
}*/
/*.top_buy{
    display: inline-block;
    height: 22px;
    line-height: 22px;
    text-align: center;
    border-radius: 10px;
    padding: 0px 16px;
    background: #1967E5;
    font-size: 14px;
    color: white;
}*/
@media screen and (max-width: 760px){
    .index-footer-bottom{
        padding-top:30px !important;
        background-color: #191D23 !important;
    }
    .header-nav-first ul{
        width: 100%;
    }
}
button{
    cursor: pointer;
    border-radius: 15px;
}
</style>
<script>
// 是否是手机
function ispcfuc() {
    var userAgentInfo = navigator.userAgent;
    var Agents = ["Android", "iPhone",
        "SymbianOS", "Windows Phone",
        "iPad", "iPod"
    ];
    var flag = true;
    for (var v = 0; v < Agents.length; v++) {
        if (userAgentInfo.indexOf(Agents[v]) > 0) {
            flag = false;
            break;
        }
    }
    return flag;
}
var ispc = ispcfuc();
</script>
<header>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-180437469-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() { dataLayer.push(arguments); }
    gtag('js', new Date());
    gtag('config', 'UA-180437469-1');
    </script>
    <nav>
        <div class="header">
            <div class="header-zhezhao"></div>
            <div class="zong">
                <div id="daohang" class="menu daohang model-2 zhong-list">
                    <div class="sanxian"></div>
                    <div class="sanxian"></div>
                    <div class="sanxian"></div>
                </div>
                <div class="zhong-list">
                    <a class="clicbot" href="<?php echo url('index/web/index'); ?>">
                        ClicBot
                    </a>
                </div>
                <div class="mobile-logo zhong-list">
                    <a style="display: none;" href="<?php echo url('index/web/index'); ?>">
                        <img class="logo" src="<?php echo htmlentities(WEB_INDEX_IMG); ?>v1/logo.png">
                    </a>
                    <!--                    <a href="https://www.amazon.com/stores/page/AB86056E-37C7-4711-A875-26DACBE455DE?channel=LP-K" target="_blank">
                       <span class="top_buy">Buy</span>
                   </a> -->
                     
                        <span class="top_buy">Buy</span>
 
                </div>
            </div>
            <div class="header-nav-first" style="">
                <ul>
                    <li><a href="<?php echo url('index/web/index'); ?>"><img class="logo" src="<?php echo htmlentities(WEB_INDEX_IMG); ?>v1/logo.png" alt=""> <span class="clicbot" style="color: white;margin-left: 20px;">ClicBot</span></a></li>
                    <li>
                        <a href="<?php echo url('web/support'); ?>">
                            <?php echo htmlentities(app('lang')->get('public_head_support')); ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo url('web/about'); ?>">
                            <?php echo htmlentities(app('lang')->get('public_head_about')); ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo url('web/contact'); ?>">
                            <?php echo htmlentities(app('lang')->get('public_head_contact')); ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?php echo url('web/referral'); ?>">
                            <?php echo htmlentities(app('lang')->get('public_head_referral')); ?>
                        </a>
                    </li>
                    <li>
                            <span class="top_buy">Buy</span>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<script type="text/javascript">
<?php

if(get_lan() == 'zh'){
    // echo 'var glo_url = "https://kybeijing-1251538927.cos.ap-beijing.myqcloud.com/clicbot"';
    // echo 'var glo_url = "http://guanwang.keyirobot.com"';
}else{
    // echo 'var glo_url = "https://kymeiguo-1251538927.cos.na-siliconvalley.myqcloud.com/clicbot"';    
}
echo 'var glo_url = "https://www.keyirobot.com";';

?>


glo_url = location.protocol + "//" + window.location.host;
var WEB_INDEX_img = glo_url + '/static/index/img';

$(function() {

    if (ispc) {
        $(".header-pc-text-top, .header-pc-top").hide();
    } else {
        $(".header-pc-text-bottom").hide();
        $(".header-pc-logo-one").hide();
    }

    if (ispc) {

        $(".header").mouseover(function() {
            $(this).css({ "background": "white" })
            $(".logo").attr("src", "<?php echo htmlentities(WEB_INDEX_IMG); ?>logo.png");
            $(".header-nav-first ul li a, .header-nav-first ul li .clicbot").css("color", "black");
        })

        $(".header").mouseout(function() {
            $(".logo").attr("src", "<?php echo htmlentities(WEB_INDEX_IMG); ?>v1/logo.png");
            $(".header").css({ "background": "none" })
            $(".header-nav-first ul li a, .header-nav-first ul li .clicbot").css("color", "white");
        })

    }

    $(".kyads-content-title-buy").click(function() {
        var jsonCallback = 'callbackpass';
        data = { 'jsonCallback': jsonCallback };
        $.ajax({
            url: 'https://hkapi.keyirobot.cn/api/user/buyclic',
            type: "GET",
            dataType: "jsonp",
            jsonp: "callbackpass",
            jsonpCallback: "callbackpass",
            data: data,
            success: function(msg) {

            }
        })
    })

    $(".head_buy").mouseleave(function() {
        return;
        $(this).addClass("head_buy_leave")
        setTimeout(function() {
            $(".head_buy").removeClass("head_buy_leave")
        }, 500)
    })

    $(".head_search").mouseover(function() {
        return;
        $(this).removeClass("head_search_leave");
        $(this).addClass("head_search_hover")
        $(this).find("img").attr("src", "<?php echo htmlentities(WEB_INDEX_IMG); ?>search_active.png")
    })

    $(".head_search").mouseleave(function() {
        return;
        $(this).removeClass("head_search_hover")
        $(".head_search").addClass("head_search_leave");
        setTimeout(function() {
            $(".head_search").find("img").attr("src", "<?php echo htmlentities(WEB_INDEX_IMG); ?>search.png")
            $(".head_search").removeClass("head_search_leave");
        }, 300)
    })

})

var glo_search_input = 0;

$(".head_search_input").on("blur", function() {
    glo_search_input = 1;
})

$(".head_search_input").on("focus", function() {
    glo_search_input = 0;
})

$(".aws_buy").click(function() {
    var jsonCallback = 'callbackpass';
    data = { 'jsonCallback': jsonCallback };
    $.ajax({
        url: 'https://hkapi.keyirobot.cn/api/user/awsbuy',
        type: "GET",
        dataType: "jsonp",
        jsonp: "callbackpass",
        jsonpCallback: "callbackpass",
        data: data,
        success: function(msg) {

        }
    })
})


// 导航菜单
$(document).on("click", "#daohang", function() {
    if ($(".header-nav-first").css("display") != "none") {
        $(".sanxian").removeClass("bar");
        $(".header-zhezhao").hide();
        $(".header-nav-first").slideUp(500);
        $(".header-nav-first").find("ul").slideUp(500);
        $("#daohang").find("img").css("margin-top", "16px");
        $(".daohang").find("img").attr("src", "<?php echo htmlentities(WEB_INDEX_IMG); ?>san.png");
        $('html,body').removeClass('stopscroll');
        $(".zong").css({ "background": "none" });
        $(".zhong-list a").css("color", "white");
        $(".header").css({ "background": "none" });
        $(".sanxian").css("backgroundColor", "white");
    } else {
        $(".sanxian").css("backgroundColor", "black");
        $(".header").css({ "background": "white" })
        $(".zong").css({ "background": "white" });
        $(".zhong-list a").css("color", "black");
        $(".top_buy").css("color", "white");
        $(".sanxian").addClass("bar");
        $(".header-nav-first").show()
        $(".header-nav-first").find("ul").slideDown(500);
        $(".header-zhezhao").show();
        $("#daohang").find("img").css("margin-top", "12px");
        $(".daohang").find("img").attr("src", "<?php echo htmlentities(WEB_INDEX_IMG); ?>close.png");
        $('html,body').addClass('stopscroll');
    }
})

// 导航菜单
$(document).on("click", ".head_top_search", function() {
    return;
    if ($(".head_search_page").css("display") != "none") {
        // $(".header-zhezhao").hide();
        $(".head_search_page").slideUp(500);
        $(".head_search_page").find("ul").slideUp(600);
        // $('html,body').removeClass('stopscroll');
    } else {
        $(".head_search_page").slideDown(500);
        $(".head_search_page").find("ul").slideDown(600);
        // $('html,body').addClass('stopscroll');
    }
})
$(window).scroll(function() {

    t = $(this).scrollTop();

    setTimeout(function() {
        b = t;
    }, 0)

    if (t > 90) {
        $(".lazy_foot").each(function() {
            if ($(this).attr("src") != $(this).attr("org_src")) {
                $(this).attr("src", $(this).attr("org_src"));
            }
        })
    }
})
setTimeout(function() {
    $(".lazy_foot").each(function() {
        if ($(this).attr("src") != $(this).attr("org_src")) {
            $(this).attr("src", $(this).attr("org_src"));
        }
    })
}, 1000)
</script>
    <div class="contact">
        <div class="contact-content">
            <img src="<?php echo htmlentities(WEB_INDEX_IMG); ?>contact/pcearth.png?bc=2">
        </div>
        <div class="contact-info">
            <ul>
                <li>Email</li>
                <li>General information: info@keyirobot.com</li>
                <li>Feedback: feedback@global.keyirobot.com</li>
                <li>For more press info: Media_Inquiries@global.keyirobot.com</li>
            </ul>
        </div>
    </div>
    </div>
    <style>
    footer {
        background-color:white;
        width: 100%;
    }
    .index-footer-top{
      display: none;
    }
    .index-footer-content {
        width: 80%;
        padding: 30px 0px;
        margin: 0px auto;
    }

 
    .index-footer-top-title{
        text-align: center;
        font-size: 42px;
        line-height: 56px;
        font-family: "DINPro-BOLD";
        color: black;
    }
 

    .index-footer-email {
        display: flex;
        flex-direction: row;
        margin-bottom: 15px;
        justify-content: space-between;
        color: black;
    }
    .index-footer-privacy a{
        color: black;
    }
.foot_email{
    background-color: white;
    color: black !important;
 }
 #semail:placeholder{
    color:red;
 }
 .index-footer-top .index-footer-top-ul{
    padding-top: 45px;
 }
    .index-footer-email li span {
        /*margin-right: 15px;*/
        position: relative;
        top: -4px;
    }

    .index-footer-email li {
        margin-right: 15px;
    }

    .index-footer-privacy {
display: flex;
    flex-direction: row;
    margin-top: 28px;
    margin-bottom: 5px;
    }
    #mc_embed_signup{
        margin-top: 30px;
    }
    .form-email-sub{
        background: black;
        color: white;
    }
    .index-footer-privacy li {
        margin-right: 20px;
    }

    .index-footer-privacy li:hover {
        cursor: pointer;
        color: white;
    }
    #semail{
        border-radius: 0px;
        padding-left: 10px;
        width:100%;
    }
    .form-email-bottom{
        width: 50%;
        position: relative;
        left: -35px;
    }
    .mc-embedded-subscribe{
        font-size: 1.5rem;

    }
    .footer-input-sub{
    line-height: 32px;
    margin-left: 10px;
    font-size: 24px;
    border-radius: 16px;
    width: 122px;
    font-family: "DINPro-Medium";
    height: 32px;
    position: relative;
    margin-top: 1px;

    }
    .lang_opt{
        cursor: pointer;
        display: inline-block;
    }
    .form-email-sub-dir{
        display: none;
    }
    .foot_two_img{
        position: relative;
        top: -40px;
        text-align: right;
    }
    .foot_two_img img{
        margin-right: 10px;
    }
    @media screen and (max-width: 768px){
        .form-email-bottom{
            left: 0px;
        }
    }

    @media screen and (max-width: 1024px){
        .form-email-bottom{
            left: 0px;
        }
    }

    @media screen and (max-width: 1260px) and (min-width: 780px){

    .index-footer-top-title{
        font-size: 2rem;
        line-height: 3rem;
     }
    }
    @media screen and (max-width: 760px){
        .kyads-content-title{
            max-width: 300px;
            width: 100%;
            text-align: center;
        }
        .index-footer-top-title{
            font-size: 1.4rem;
            line-height: 2rem;
            margin-bottom: 10px;
        }
        .form-email-bottom{
            margin: 0px;
            left: 0px;
        }
        .kyads-content-title{
            top: 30%;
        }

        .kyads-content-title1{
            margin: 0 auto;
            margin-bottom: 0px;
            line-height: 38px;
        }
        .kyads-content-title2{
            margin: 0 auto;
        }
        .footer-hr{
            display: none;
        }
        .index-footer-privacy{
            flex-wrap: wrap;
        }
        .index-footer-privacy li{
            margin-bottom: 20px;
        }
        .index-footer-top-title{
            text-align: left;
        }
     .index-footer-top-title{
    font-size: 16px;
    line-height: 20px;
 }
 #semail{
    width: 140px;
    margin-top: 6px;
 }
 .footer-input-sub{
    line-height: 25px;
    height: 25px;
    font-size: 16px;
 }
 .footer-input-sub{
    width: 90px;
    text-align: center;
 }
.index-footer-content{
    width: 100%;
}
.footer-input-shu{
    display: none;
}
.index-footer-bottom{
    background-color: black;
    color: white;
    padding: 0px 5%;
  }
.index-footer-privacy a{
    color: white;
}
.index-footer-top{
    width: 90%;
    margin: 0 auto;
    display: none;
}
.index-footer-email{
    color: white;
}
.index-footer-content{
    padding: 0px;
}
    }
    @media screen and (max-width: 360px){
        .index-footer-email li{
            margin-right: 0px;
        }
        .index-footer-privacy li{
            margin-bottom: 10px;
        }
    }
</style>
<footer>
    <div class="index-footer-content">
        <div class="index-footer-top">
            <ul class="index-footer-top-ul">
                <li class="index-footer-top-title">
                    <?php echo htmlentities(app('lang')->get('index_foot_email1')); ?>
                </li>
                <li class="index-footer-top-title">
                    <?php echo htmlentities(app('lang')->get('index_foot_email2')); ?>
                </li>
                <li class="index-footer-top-title">
                    <?php echo htmlentities(app('lang')->get('index_foot_email3')); ?>
                </li>
                <li>
                    <div id="mc_embed_signup">
                        <form action="https://cell-robot.us9.list-manage.com/subscribe/post?u=77776a740ac9daca1572b2e83&id=aeca02c56c" class="validate" id="mc-embedded-subscribe-form" method="post" name="mc-embedded-subscribe-form" novalidate="" target="_blank">
                            <div class="form-email">
                                <div>
                                    <ul class="form-email-bottom" style="display: flex;">
                                        <li class="form-email-li-input">
                                            <span class="footer-input-shu">
                                            </span>
                                            <input class="foot_email required email" id="semail" name="EMAIL" placeholder="<?php echo htmlentities(app('lang')->get('index_p12_your')); ?>" style="color: white;" type="email" value="">
                                            </input>
                                        </li>
                                        <li style="width:5px;">
                                        </li>
                                        <li class="form-email-li-sub">
                                            <!-- <span><img src="<?php echo htmlentities(WEB_INDEX_IMG); ?>foot_email.png" style="width: 25px;cursor: pointer;"></span> -->
                                            <span class="form-email-sub-dir">
                                            </span>
                                            <input class="form-email-sub footer-input-sub" id="mc-embedded-subscribe" name="subscribe" style="" type="submit" value="<?php echo htmlentities(app('lang')->get('index_p12_sub')); ?>">
                                            </input>
                                        </li>
                                        <li id="resmessage" style="display: none;">
                                            Subscription successful!
                                        </li>
                                    </ul>
                                </div>
                                <div class="clear sub-title footer-sub-title" id="mce-responses">
                                    <div class="response" id="mce-error-response" style="color:white;font-size:12px;width:100%;margin:0 auto;text-align: left;display: none;">
                                        <!--              By clicking the button to subscribe, you acknowledge that you have authorized us to send you email, direct mail and customized online advertising. You can unsubscribe at anytime by clicking the link in the footer of our email. -->
                                    </div>
                                    <div class="response" id="mce-success-response" style="display:none">
                                    </div>
                                </div>
                                <!--                   <div class="sub-title footer-sub-title" style="display: none;">
                                By clicking the button to subscribe, you acknowledge that you have authorized us to send you email, direct mail and customized online advertising. You can unsubscribe at anytime by clicking the link in the footer of our email.
                            </div> -->
                                <div>
                                    <div class="mergeRow gdpr-mergeRow content__gdprBlock mc-field-group" id="mergeRow-gdpr">
                                        <div class="content__gdpr">
                                            <fieldset class="mc_fieldset gdprRequired mc-field-group" name="interestgroup_field" style="border:none;display:none;">
                                                <label class="checkbox subfield" for="gdpr_31918">
                                                    <input checked="" class="av-checkbox " id="gdpr_31918" name="gdpr[31918]" type="checkbox" value="Y">
                                                    <span style="color:white;">
                                                        Agree
                                                    </span>
                                                    </input>
                                                </label>
                                                <label class="checkbox subfield" for="gdpr_31922">
                                                </label>
                                            </fieldset>
                                        </div>
                                        <div class="content__gdprLegal">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="mc_embed_signup_scroll">
                                <div class="indicates-required">
                                </div>
                                <div class="mc-field-group">
                                </div>
                                <div class="mc-field-group" style="display:none;">
                                    <label for="mce-FNAME">
                                        First Name
                                    </label>
                                    <input class="" id="mce-FNAME" name="FNAME" type="text" value="">
                                    </input>
                                </div>
                                <div class="mc-field-group" style="display:none;">
                                    <label for="mce-LNAME">
                                        Last Name
                                    </label>
                                    <input class="" id="mce-LNAME" name="LNAME" type="text" value="">
                                    </input>
                                </div>
                                <div class="mc-field-group" style="display:none;">
                                    <label for="mce-MMERGE5">
                                        Zip Code
                                    </label>
                                    <input class="" id="mce-MMERGE5" name="MMERGE5" type="text" value="">
                                    </input>
                                </div>
                                <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                                <div aria-hidden="true" style="position: absolute; left: -5000px;">
                                    <input name="b_77776a740ac9daca1572b2e83_aeca02c56c" tabindex="-1" type="text" value="" />
                                </div>
                            </div>
                        </form>
                    </div>
                </li>
            </ul>
        </div>
        <div class="index-footer-bottom">
            <ul class="index-footer-email">
                <li>
                    Email: <?php echo htmlentities(app('lang')->get('index_foot_email')); ?>
                </li>
                <li style="display: none;">
                    <p id="lang_zh" class="lang_opt">中文 / </p>
                    <p id="lang_en" class="lang_opt">English</p> / <p class="lang_opt" id="lang_ja">日本語</p>
                </li>
            </ul>
            <div class="footer-hr">
                <hr>
                </hr>
            </div>
            <ul class="index-footer-privacy">
                <li>
                    <a href="<?php echo url('index/web/warranty'); ?>">
                        Warranty Policy
                    </a>
                </li>
                <li>
                    <a href="<?php echo url('index/web/privacy'); ?>">
                        Privacy Policy
                    </a>
                </li>
                <li>
                    <a href="<?php echo url('index/web/refund'); ?>">
                        Refund Policy
                    </a>
                </li>
                <li>
                    <a href="<?php echo url('index/web/shipping'); ?>">
                        Shipping Policy
                    </a>
                </li>

                <li>
                    <a href="<?php echo url('index/web/Conditions'); ?>">
                       Terms and Conditions
                    </a>
                </li>
            </ul>
            <ul class="foot_two_img">
                <li>
                    <span>
                        <a href="https://www.facebook.com/keyirobot/" target="_blank">
                            <img class="lazy_foot" org_src="<?php echo htmlentities(WEB_INDEX_IMG); ?>facebook.png" style="width: 18px;height: 18px;" />
                        </a>
                    </span>
                    <span>
                        <a href="https://twitter.com/KeyiTech" target="_blank">
                            <img class="lazy_foot" org_src="<?php echo htmlentities(WEB_INDEX_IMG); ?>twitter.png" style="width: 18px;height: 18px;" />
                        </a>
                    </span>
                </li>
            </ul>
        </div>
    </div>
    <div style="padding: 20px 0px;text-align:center;">   
        <span>Copyright © 2021 北京可以科技有限公司. All rights reserved.</span>&nbsp;
    <span>CELL ROBOTICS HK LIMITED:  </span>  <span>   UNIT 3A 12/F KAISER CENTRE NO·18 CENTRE STREET SAI YING PUN </span>
    </div>
    <div id="index_bottom_top"></div>
</footer>
<script src="<?php echo htmlentities(WEB_INDEX_JS); ?>email.js" type="text/javascript"></script>
<script>
$(function() {

    //订阅邮件
    function eamilcheck(val) {
        var myreg = /^([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|\_|\.]?)*[a-zA-Z0-9]+\.[a-zA-Z]{2,3}$/;
        if (!myreg.test(val)) {
            return false;
        } else {
            return true;
        }
    }

    var flag = true;

    $("#mc-embedded-subscribe").on("click", function() {
        var val = $("#semail").val();
        if (!eamilcheck(val) && val) {
            $("#mce-error-response").html("Please examine the email address. Thinks!");
            $("#mce-error-response").show();

        } else if (!val) {
            $(".footer-input-shu").hide();
            $("#mce-error-response").html("By clicking the button to subscribe, you acknowledge that you have authorized us to send you email, direct mail and customized online advertising. You can unsubscribe at anytime by clicking the link in the footer of our email.");
            $("#mce-error-response").show();
        }
    })

    $(".foot_email, #semail").focus(function() {
        var val = $("#semail").val();
        val = val.replace(/^\s*|\s*$/g, "");
        $("#mce-error-response").css({ "color": "black" });
        if (!val) {
            $(".footer-input-shu").hide();
            $("#mce-error-response").html("By clicking the button to subscribe, you acknowledge that you have authorized us to send you email, direct mail and customized online advertising. You can unsubscribe at anytime by clicking the link in the footer of our email.");
            $("#mce-error-response").fadeIn(500);
        } else if (!eamilcheck(val) && val) {
            $("#mce-error-response").html("Please examine the email address. Thinks!");
            $("#mce-error-response").fadeIn(500);

        }
    })

    $(".foot_email").on("blur", function() {
        $(".footer-input-shu").show();
        $("#mce-error-response").fadeOut(1000);
    })

})

$(document).ready(function() {
    // 解决刷新浏览器的bug , 每次刷新浏览器返回到顶部导航    
    // 解决刷新浏览器的bug , 每次刷新浏览器返回到顶部导航    
    // setTimeout(function() {
    //     $("html,body").animate({ scrollTop: 0 }, 20);
    glo_first_load = 1;
    //     // $("#product_animate").css({ "position": "relative" })
    // }, 600)
    // $("html,body").animate({ scrollTop: 0 }, 20);
    // 解决刷新浏览器的bug
    // $("#product_animate").css({ "position": "relative" })
})



// 控制首页视频播放按钮的动画
$(".head_video_san_bg").mouseover(function() {
    $(this).removeClass("head_video_san_bg_short")
    $(this).addClass("head_video_san_bg_long");
    $(".head_video_san").css("border-left", "7px solid white");
})

$(".head_video_san_bg").mouseleave(function() {
    $(this).addClass("head_video_san_bg_short");
    $(".head_video_san").css("border-left", "7px solid #396BE4");
    $(this).removeClass("head_video_san_bg_long");
})

$(".head_video_san_txt").mouseleave(function() {
    if ($(".head_video_san_bg").hasClass("head_video_san_bg_long")) {
        $(this).addClass("head_video_san_bg_short");
        $(".head_video_san").css("border-left", "7px solid #396BE4");
        $(this).removeClass("head_video_san_bg_long");
    }
})

// 控制提交邮箱按钮背景色
$(".footer-input-sub, .form-email-sub-dir").mouseover(function() {
    $(".form-email-sub-dir").css({ "border-top": "2px solid white", "border-right": "2px solid white" });
    $(".form-email-sub").css({ "backgroundColor": "#555" })
})
$(".footer-input-sub").mouseleave(function() {
    $(".form-email-sub-dir").css({ "border-top": "2px solid #343434", "border-right": "2px solid #343434" });
    $(".form-email-sub").css({ "backgroundColor": "black" })
})

function formatInt(number, len) {
    var mask = "";
    var returnVal = "";
    for (var i = 0; i < len; i++) mask += "0";
    returnVal = mask + number;
    returnVal = returnVal.substr(returnVal.length - len, len);
    return returnVal;
}


(function($) {
    window.fnames = new Array();
    window.ftypes = new Array();
    fnames[0] = 'EMAIL';
    ftypes[0] = 'email';
    fnames[1] = 'FNAME';
    ftypes[1] = 'text';
    fnames[2] = 'LNAME';
    ftypes[2] = 'text';
    fnames[3] = 'ADDRESS';
    ftypes[3] = 'address';
    fnames[4] = 'PHONE';
    ftypes[4] = 'phone';
    fnames[5] = 'MMERGE5';
    ftypes[5] = 'zip';
}(jQuery));
var $mcj = jQuery.noConflict(true);
</script>
<script type="text/javascript">
var glo_ajax_count = 0;
var glo_ajax_set;
var glo_ajax_time_org = Date.parse(new Date());
var glo_ajax_time = '';


function navflag() {
    // 获取 导航选中状态
    var strUrl = window.location.href;
    var arrUrl = strUrl.split("/");
    var strPage = arrUrl[arrUrl.length - 1];
    var file = strPage.split(".");
    filename = file[0];
    // var arr = ['index', 'support', 'about', 'contact'];
    // var index = arr.indexOf(filename);
    // return index;
    return filename;
}

// function ajax_set() {
//     ++glo_ajax_count;
//     if (glo_ajax_count > 10) {
//         clearInterval(glo_ajax_set);
//         return;
//     }
//     var data = {
//         "glo_ajax_count": glo_ajax_count,
//         "plat": iosand(),
//         "act": navflag(),
//         "appCodeName": navigator.appCodeName,
//         "appName": navigator.appName,
//         "appVersion": navigator.appVersion,
//         "cookieEnabled": navigator.cookieEnabled,
//         "platform": navigator.platform,
//         "agent": navigator.userAgent,
//         'glo_ajax_time_org': glo_ajax_time_org,
//         'glo_ajax_time': Date.parse(new Date()),
//     };
//     $.ajax({
//         url: '<?php echo url("index/web/ajax_set"); ?>',
//         type: "POST",
//         data: { data: data },
//         success: function(msg) {
//             console.log(msg)
//         }
//     })
// }

// 设备：
function iosand() {
    var userAgentInfo = navigator.userAgent;
    if (userAgentInfo.indexOf('Android') > 0) {
        return 'Android';
    } else if (userAgentInfo.indexOf('iPhone') > 0) {
        return 'iPhone';
    } else if (userAgentInfo.indexOf('SymbianOS') > 0) {
        return 'SymbianOS';
    } else if (userAgentInfo.indexOf('Windows Phone') > 0) {
        return 'Windows Phone';
    } else if (userAgentInfo.indexOf('iPad') > 0) {
        return 'iPad';
    } else if (userAgentInfo.indexOf('iPod') > 0) {
        return 'iPod';
    } else {
        return 'PC';
    }
}

function setCookie(key, value, t) {
    var oDate = new Date();
    oDate.setDate(oDate.getDate() + t);
    document.cookie = key + "=" + value + "; expires=" + oDate.toDateString();
}


$(function() {

    $(".lang_opt").click(function() {
        var index = $(this).index();
        if (index == 1) {
            setCookie('think_var', 'en', 86400 * 7);
        } else if (index == 2) {
            setCookie('think_var', 'ja', 86400 * 7);
        } else if (index == 0) {
            setCookie('think_var', 'zh', 86400 * 7);
        }
        setTimeout(function() {
            window.location.reload();
        }, 100);
    })

    $(window).scroll(function() {
        t = $(this).scrollTop();
        $(".header-nav-first, .zong").css({ "top": -t })
    })

})
</script>
<!-- Facebook Pixel Code -->
<script>
! function(f, b, e, v, n, t, s) {
    if (f.fbq) return;
    n = f.fbq = function() {
        n.callMethod ?
            n.callMethod.apply(n, arguments) : n.queue.push(arguments)
    };
    if (!f._fbq) f._fbq = n;
    n.push = n;
    n.loaded = !0;
    n.version = '2.0';
    n.queue = [];
    t = b.createElement(e);
    t.async = !0;
    t.src = v;
    s = b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t, s)
}(window, document, 'script',
    'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '1035172393573004');
fbq('track', 'PageView');
</script>
<noscript>
<img height="1" width="1" src="https://www.facebook.com/tr?id=1035172393573004&ev=PageView&noscript=1" />
</noscript>
<!-- End Facebook Pixel Code -->

<!-- Global site tag (gtag.js) - Google Ads: 461338622 -->
<script type="text/javascript">

$(".top_buy, .min_buy, .bottom_buy").click(function(){
    gtag_report();
})

function gtag_report() {
    gtag('event', 'click', { 'event_category': 'amazon' });
    // window.location = 'https://www.amazon.com/stores/page/AB86056E-37C7-4711-A875-26DACBE455DE?channel=LP-K';
    var country = '<?php echo htmlentities($country); ?>';
    country = country.replace('&amp;', '&');
    window.location = country;

}
 
</script>

<script>
window.addEventListener('DOMContentLoaded', function(e) {
    if(document.querySelector("#index-sub-float-email #index-top-sub") !== null){
        document.querySelector("#index-sub-float-email #index-top-sub").addEventListener("click", function(e){
            if(document.querySelector("#index-sub-float-email #sub_email").value != ""){
                gtag('event', 'click', {'event_category' : 'subscribe'});
            }
        })
    }
});
</script>

<!-- Cookie Consent by https://www.TermsFeed.com -->
<script type="text/javascript" src="//www.termsfeed.com/public/cookie-consent/3.1.0/cookie-consent.js"></script>
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function () {
cookieconsent.run({"notice_banner_type":"headline","consent_type":"express","palette":"dark","language":"en"});
});
</script>

<noscript>Cookie Consent by <a href="https://www.TermsFeed.com/" rel="nofollow noopener">TermsFeed</a></noscript>
<!-- End Cookie Consent -->
</body>

</html>